#include "enigma.hpp"

using namespace std;

/*
wheel left;
wheel center;
wheel right;
reflector UKW;
plugboard pb;
*/

//encoding and decoding on the enigma was the same thing
string enigma::encode(const string& input)
{
	return decode(input);
}

//takes the values from the input string and treats them as if they were being typed into the enigma to return an encrypted string
string enigma::decode(const string& input)
{
	return "";
}

//creates the machine
enigma::enigma(const char& leftIn, const char& centerIn, const char& rightIn, const string& plugboardIn)
{
	left = wheel("", leftIn);
	center = wheel("", centerIn);
	right = wheel("", rightIn);
	pb = plugboard(plugboardIn);
	UKW = reflector();
}

//
void enigma::setup(const char& leftIn, const char& centerIn, const char& rightIn, const string& plugboardIn)
{
	left.rotate(leftIn);
	center.rotate(centerIn);
	right.rotate(rightIn);
	pb.set(plugboardIn);
}