#include "enigma.hpp"
#include "subparts/wheel.hpp"
#include <iostream>

using namespace std;

void to_lower(string& data)
{
	for(auto iter = data.begin();iter!=data.end();iter++){
		*iter = tolower(*iter);
	}
}

int main()
{


	//wheel test("abcdefghijklmnopqrstuvwxyz", 'a');
	string firstwheel = "JGDQOXUSCAMIFRVTPNEWKBLZYH";
	to_lower(firstwheel);
	wheel test(firstwheel, 'a');
	//cout<< test.in('a')<<endl;
	return 0;
}

