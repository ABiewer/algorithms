#pragma once
#include <iostream>
#include "subparts/wheel.hpp"
#include "subparts/plugboard.hpp"
#include "subparts/reflector.hpp"

using namespace std;

class enigma
{
public:
	wheel left;
	wheel center;
	wheel right;
	reflector UKW;
	plugboard pb;

	enigma(const char& leftIn, const char& centerIn, const char& rightIn, const string& plugboardIn);

	string encode(const string& input);
	string decode(const string& input);

	void setup(const char& leftIn, const char& centerIn, const char& rightIn, const string& plugboardIn);
};