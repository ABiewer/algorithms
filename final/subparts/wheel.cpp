#include "wheel.hpp"
#include <string>

using namespace std;

//gets the integer value for the letter, with 'a' == 0
inline int index(const char& letter)
{
	return letter - 'a';
}

wheel::wheel()
{
	//dummy constructor to get the compiler to stop complaining
}

/**
 * Creates a wheel with the given inputs. 
 * @param inputString the mapping going from the keyboard toward the reflector
 * @param initialRotation the initial position of the wheel 
 * */
wheel::wheel(const string& inputString, const char& initialRotation)
{
	//input checking
	if(inputString.size()<25 || initialRotation < 'a' || initialRotation > 'z')
	{
		string throwed = "Invalid parameters: " + inputString;
		throwed += " , " + initialRotation;
		throw throwed;
	}

	rotation = index(initialRotation);

	//set the values for the mappings
	for(int i = 0;i<26;i++){
		mapping[i] = inputString[i];
	}
	
	createReverseMapping();
}

//if going toward the reflector, use the in command to get the next letter
inline char wheel::in(const char& input)
{
	return mapping[index(input)];
}

//if going away from the reflector, use the out command to get the next letter
inline char wheel::out(const char& input)
{
	return reverseMapping[index(input)];
}

//rotates the wheel by one rotation
void wheel::rotate()
{
	rotation = (rotation + 1)%26;
}

//used to initialize the wheel before decryption/encryption
void wheel::rotate(const char& letter)
{
	int rot = index(letter);
	if(rot<0||rot>25)
	{
		//what to do if the rotation is invalid?
	}

	rotation = rot;
}

//creates the reverse mapping thru the wheel. If the keyboard 'a' translates to 
//'d', then the reversemapping will tell us that a 'd' coming from the reflector 
//should be turned into an 'a'. 
void wheel::createReverseMapping()
{
	for(int i = 0;i<26;i++){
		reverseMapping[index(mapping[i])] = ('a' + i);
	}
}
