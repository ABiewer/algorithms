#pragma once
#include <string>

using namespace std;

struct wheel
{
public:
	char mapping[26];
	char reverseMapping[26];
	int rotation;

	wheel();
	//creates the wheel with the mapping
	wheel(const string& inputString, const char& initialRotation);

	char in(const char& input);
	char out(const char& input);

	void rotate();
	void rotate(const char& letter);
	void createReverseMapping();
};