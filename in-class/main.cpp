#include <iostream>
#include <vector>
#include "time.h"

//Global constants in all caps
const int NUM_BEADS = 25;
const int NUM_COLORS = 2;

using namespace std;

//A function that takes in a collection of bracelents with n beads,
//returns bracelents with n+1 beads.
vector< vector <int> > add_bead(vector< vector<int> >& bracelets, int numColors);

//A function that takes in a bracelet and returns a list of bracelets
//that are all cyclic rotations of that bracelet.
vector <vector <int>> same_bracelets(vector<int>& bracelet);

//Show me the ACTUAL bracelets
void print_bracelets(vector <vector <int> >& bracelets);

int main()
{
	time_t start_time = time(NULL);
	vector<vector<int> > test;
	test.push_back({});
	//create all possible bracelets without symmetry.
	for (int i = 0; i < NUM_BEADS; i++)
		test = add_bead(test, NUM_COLORS);

	time_t end_time = time(NULL);
	cout << "This took " << end_time - start_time << " seconds." << endl;
	start_time = end_time;

	/*
	//a test to see if our same_bracelets function works.
	//it does!
	vector <int> trial = { 0,0,0,1 };
	vector <vector <int>> cycles = same_bracelets(trial);

	//Let's go through the possible permutations and remove duplicates.
	for (size_t i = 0; i < test.size(); i++)
	{
		//get all of the rotations of one bracelet
		vector<vector<int>> rotations = same_bracelets(test.at(i));

		for (size_t j = i+1; j < test.size(); j++)
		{
			//if (j == i)
				//continue;

			//go through all of the elements after the one we are at and check if there is a repeat
			for (size_t k = 0; k < rotations.size(); k++)
			{

				if (rotations.at(k) == test.at(j))
				{
					test.erase(test.begin() + j);
					j--;
					break;
				}
			}
		}
	}

	end_time = time(NULL);
	cout << "The number of bracelets is " << test.size() << endl;
	cout << "This took " << end_time - start_time << " seconds." << endl;
	//print_bracelets(cycles);

	system("pause");
	*/
	return 0;
}


void print_bracelets(vector <vector <int> >& bracelets)
{
	for (int i = 0; i < bracelets.size(); i++)
	{
		for (int j = 0; j < bracelets[i].size(); j++)
		{
			cout << bracelets[i][j] << ' ';
		}
		cout << endl;
	}
}
vector< vector <int> > add_bead(vector< vector<int> >& bracelets, int numColors)
{
	vector < vector <int> > ret_vals;

	//for each color
	for (int col = 0; col < numColors; col++)
	{
		//for each existing bracelet
		for (int brace = 0; brace < bracelets.size(); brace++)
		{
			vector<int> newBracelet;
			newBracelet = bracelets.at(brace);
			newBracelet.push_back(col);
			ret_vals.push_back(newBracelet);
		}
	}

	return ret_vals;
}
vector <vector <int>> same_bracelets(vector<int>& bracelet)
{
	vector <vector <int>> ret_vals;
	auto temp = bracelet;

	for (int i = 0; i < bracelet.size(); i++)
	{
		int back = temp[temp.size() - 1];
		temp.pop_back();
		temp.insert(temp.begin(), back);
		ret_vals.push_back(temp);
	}

	return ret_vals;

}