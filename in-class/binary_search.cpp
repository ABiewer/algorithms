#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int to_be_searched[10000];
    for(int i = 0;i<10000;i++){
        to_be_searched[i] = i;
    }
    to_be_searched[12] = 13;
    int min = 0;
    int max = sizeof(to_be_searched) / sizeof(int);
    int arrayMax = max;

    srand(time(NULL));

    int goal = rand()%10000;
    goal = 12;
    int nsteps = 0;

    while(true)
    {
        nsteps++;
        int guess = (max+min)/2;
        if(goal>guess)
        {
            min = guess+1;
            continue;
        }
        if(goal<guess)
        {
            max = guess-1;
            continue;
        }
        if(goal!=max){ //max==min, if max and min overlap, but the goal is not the same, then it is not in the list
            cout<<"The number was not in the list"<<endl;
            break;
        }
        //if(goal==guess)
        cout<<"The correct number is: "<<goal<<" and the guess reached the goal in "<<nsteps<<" steps."<<endl;
        break;
    }

}