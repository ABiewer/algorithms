#include "PrimeFinder.hpp"
#include <cmath>	//sqrt
#include <chrono>	//timing
#include <iostream>	//cout
#include <stdlib.h>	//rand, srand
#include <time.h>	//time(NULL)
#include <vector>	

#ifndef long_long_ll
#define long_long_ll
typedef unsigned long long ll;
#endif

using namespace std;

/**
 * Prime Finder, the 'interface' for finding primes. 
 * Constructer doesn't do anything
 * */
PrimeFinder::PrimeFinder()
{
}

/**
 * Finds out if the @param prime is a prime number using a 'brute force' search for every value between 2 and sqrt(prime)
 * @return whether the prime is actually prime
 * */
inline bool PrimeFinder::brute_force_is_prime(const ll &prime)
{
	ll sqrtINT = (ll)sqrt(prime);

	for (ll i = 2; i <= sqrtINT; i++)
		if ((prime / i) * i == prime)
			return false;

	return true;
}

/**
 * Finds out if the @param prime is a prime number using a Monte Carlo search for 20 values between 1 and the prime using 
 * Fermat's Little Theorem
 * @return whether the prime is actually prime
 * */
inline bool PrimeFinder::monte_carlo_is_prime(const ll &prime)
{
	const ll &n = prime;
	srand(time(NULL));
	for (int i = 0; i < 20; i++)
	{
		ll a = ((ll)rand()) % (n - 1) + 1;
		//test Fermat's theorem on the prime and the random number 'a'
		if (FermatLittleTheorem(a, n) != 1)
			return false;
	}
	return true;
}

/**
 * Calls both the brute force and monte carlo methods to determine if the @param prime is a prime number. 
 * @return nothing
 * */
void PrimeFinder::test_both_is_prime(const ll &prime)
{
	//start time
	int msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	if (brute_force_is_prime(prime))
		cout << "Brute force says " << prime << " is prime." << endl;
	else
		cout << prime << " is a composite number via Brute Force." << endl;

	//end time
	int msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout << "The brute force calculation took " << ((double)(msEnd - msStart)) / 1000.0 << " seconds to compute." << endl;

	//start time
	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	if (monte_carlo_is_prime(prime))
		cout << "Monte Carlo says " << prime << " is prime." << endl;
	else
		cout << prime << " is a composite number via Monte Carlo." << endl;

	//end time
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout << "The Monte Carlo calculation took " << ((double)(msEnd - msStart)) / 1000.0 << " seconds to compute." << endl
		 << endl;
}

/**
 * Uses Fermat's Little Theorem to determine whether the number @param n is prime by finding a^(n-1)%n == 1
 * Uses an algorithm to find values for a^2^power quickly, up to the value for 2^power==n-1, and finds a^(n-1) 
 * quickly using those values. 
 * @return a^(n-1)%n == 1
 * */
ll PrimeFinder::FermatLittleTheorem(const ll &a, const ll &n)
{
	//the value for a must be smaller than n
	if (a >= n)
		return 0;

	//start by finding the largest power of 2 in n-1
	ll finalPower = 1;
	ll twoRaisedToPower = 2;
	while ((n - 1) >= twoRaisedToPower && finalPower < 64)
	{
		finalPower++;
		twoRaisedToPower *= 2;
	}
	if (finalPower > 32)
	{
		cout << "The largest value for unsigned-long long is 2^64, and the numbers being multiplied in the Fermat Theorem are larger than sqrt(2^64), so "
			 << "the theorem may not predict the correct result." << endl;
	}

	//go backwards one power of 2
	finalPower--;
	twoRaisedToPower /= 2;

	//now make a list of a^2^power from power=0 to power == finalPower
	vector<ll> aPowersOfTwo;
	//the first value will be a^2^0 == a
	aPowersOfTwo.push_back(a);
	for (ll power = 1; power <= finalPower; power++)
		aPowersOfTwo.push_back((aPowersOfTwo[power - 1] * aPowersOfTwo[power - 1]) % n);

	//with the list of values needed in aPowersOfTwo, go through and multiply the
	//correct values to get n as powers of two
	ll tempN = n - 1;
	ll retval = 1;
	while (finalPower > 0)
	{
		//if 2^power can be subtracted from n-1, then it should be multiplied into the result.
		if (tempN >= twoRaisedToPower)
		{
			tempN -= twoRaisedToPower;
			retval = (retval * aPowersOfTwo[finalPower]) % n;
		}
		//decrement the power
		twoRaisedToPower /= 2;
		finalPower--;
	}

	return retval % n;
}