/*
	Adam Biewer
	Algorithms - Jensen
	Assignment 3: Prime Checking using Monte Carlo and Fermat's Little Theorem
*/

/*
Objective: In this homework, you will write a computer program that checks to
determine if a number is prime in two different ways:
1. Directly, via checking for prime factors up to the square root of the number
2. Indirectly, through a Monte Carlo method using Fermat’s Little Theorem.
A Monte Carlo algorithm is one that is much faster than other algorithms because ...
(drumroll) it isn’t an algorithm! It is a probabilistic test that is wrong very infrequently
but has a significantly faster run time. Fermat’s Little Theorem states:
Theorem 1. If p is prime and 1 ≤ a < p is relatively prime to p, then a ^ (p−1) % p = 1.
There’s no need to be too concerned about the word relatively prime. In general, two
numbers are relatively prime if the only number to divide both of them is 1. A prime
number is relatively prime to any number smaller than it, and we are checking for primes,
so this part of the theorem is vacuously satisfied for prime numbers.
The following is our Monte Carlo algorithm for determining if a number n is prime,
even when n is very large.
1. Pick a random sample of numbers greater than 1 and less than n.
2. For each randomly selected number a, compute a ^ n−1 % n.
3. If the previous result is ever NOT 1, the number is composite.
4. If all computations in step 2 are 1, deem the number prime.
One question still remains about this method, which is how many elements you need
to pick to have a random sample. There is another mathematical theorem on our side,
however, telling us that when a number is composite, at least half of the values smaller
than it are NOT relatively prime to that number. Hence if we pick k values at random
less than n, the probability that ALL of them are relatively prime to n is less than 1 ^ 2 ^ k. 
When k is somewhat small, say even 10, this probability is less than .09%.

Program Requirements: Your program should use a long long to represent an
integer input by the user. Your program should check to make sure the value entered
is greater than 1. Your program should then run both tests on the value input by the
user to determine whether or not the value is prime. It should also output the number
of seconds needed to do each computation for comparison purposes. As a few small
examples:
1. When 7919 is input, both methods should output that this number is prime in 0
seconds.
2. When 5400 is input, both methods should output that this number is composite in
0 seconds.
3. When 8911 is input, the algorithm will indicate that this number is composite,
while the Monte Carlo method will indicate that this number is prime.
Thoughts / Additional Information:
1. Computing a
n−1%n should be done efficiently via a divide and conquer algorithm
in order for the Monte Carlo method to have any computational savings.
2. To avoid overflow issues, your algorithm for computing a
n−1%n should always mod earlier rather than later. For example, When computing 34
(mod 5), you would want to store the values 3, 4, and 1 rather than 3, 9, and 81.
3. According to the Prime Number Theorem, approximately 1/ log n values less than
n are prime. You may want to do a sieve of Eratosthenes to filter out for primes
once upon starting the program to only have to check for divisibility by prime
numbers in your first algorithm, but that choice is up to you.
*/

#include <iostream>
#include "PrimeFinder.hpp"

#ifndef long_long_ll
#define long_long_ll
typedef unsigned long long ll;
#endif

using namespace std;

int main(int argc, char** argv)
{
	//ll = unsigned long long, 64 bytes on x86
	ll input = 0;
	//check for commandline arguments
	try{
		if(argc>1)
			input = stoull(argv[1]);
	}
	catch (invalid_argument)
	{
		//catches exist if the inputs exceed the length of a long long, or if there are invalid characters beginning the string
		cout << "Could not convert the input to a long long. " << endl;
		system("pause");
		return 0;
	}
	catch (out_of_range)
	{
		cout << "Could not convert the input to a long long. " << endl;
		system("pause");
		return 0;
	}

	if(input == 0){
		string line = "";
		cout<<"Please enter a number you wish to know is prime: ";
		getline(cin, line);
		cout<<endl;
		try{
			input = stoull(line);
			if(input==0){
				cout<<"Please enter a value greater than 0. "<<endl;
				system("pause");
				return 0;
			}
		}
		catch(invalid_argument)
		{
			cout<<"Could not convert the input to a long long. "<<endl;
			system("pause");
			return 0;
		}
		catch(out_of_range){
			cout<<"Could not convert the input to a long long. "<<endl;
			system("pause");
			return 0;
		}
	}
	//and finally, checking if it's prime (both via brute force 1<x<sqrt(prime) and via 20 guesses at Fermat's Theorem)
	PrimeFinder pf;
	pf.test_both_is_prime(input);
	system("pause");
	return 0;
}