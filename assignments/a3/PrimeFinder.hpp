#pragma once

#ifndef long_long_ll
#define long_long_ll
typedef unsigned long long ll;
#endif

using namespace std;

struct PrimeFinder
{
public:
	PrimeFinder();

	bool brute_force_is_prime(const ll &prime);
	bool monte_carlo_is_prime(const ll &prime);
	void test_both_is_prime(const ll &prime);
	ll FermatLittleTheorem(const ll &a, const ll &n);
};