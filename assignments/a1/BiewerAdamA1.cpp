/**
 * Adam Biewer
 * CSC3750 Algorithms - Jensen
 * 02/16/2020
 * 
 * Towers assignment
 * Input file will be a bunch of integers representing the rows and columns and 0's for the blank spots.
 * Output should be a picture/console log of a solved puzzle or should state that the combination is impossible.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

struct Skyscraper
{
public:
	vector<vector<int>> sides;
	vector<int> grid;
	const int BOARDLEN = 4;

	/**
	 * Constructor for a Skyscraper. Will take in vector<vector<int>> sides which represents the sides of the skyscraper.
	 * The sides are taken in order from the top left, moving clockwise around the sides of the board. 
	 * */
	Skyscraper(vector<vector<int>> input)
	{
		swap(sides, input); //sides = input;
		if (sides.size() != (size_t)BOARDLEN)
		{
			throw "An invalid vector has been passed into the Skyscraper constructor.";
		}
		for (auto i : sides)
		{
			if (i.size() != (size_t)BOARDLEN)
			{
				throw "An invalid vector has been passed into the Skyscraper constructor.";
			}
		}
		grid = vector<int>(BOARDLEN * BOARDLEN, 0);
	}

	/**
	 * This function will solve the skyscraper and return true, or will find that there are no possible solutions, and will return false;
	 * */
	bool solve()
	{
		if (!recursive_solve())
			return false;
		return true;
	}

	void print_board()
	{
		print_board(grid);
	}

	/**
     * Print out the board to the command line in the format of a skyscrapper puzzle. (e.g.:)
     *    2 2 1 2  
     *    -------
     * 2 |2 1 4 3| 2
     * 2 |1 4 3 2| 3
     * 1 |4 3 2 1| 4
     * 2 |3 2 1 4| 1
     *    -------  
     *    2 3 4 1
     * */
	void print_board(const vector<int> &tempGrid)
	{
		if (tempGrid.size() != (size_t)(BOARDLEN*BOARDLEN))
		{
			throw "Invalid argument to print_board";
		}
		cout << "   ";
		for (auto iter = sides[0].begin(); iter != sides[0].end(); iter++)
			cout << *iter << " ";
		cout << endl;

		cout << "   -------" << endl;

		//row 1
		cout << sides[3][3] << " |";
		for (int i = 0; i < 4; i++)
			cout << tempGrid[i] << " ";
		cout << "| " << sides[1][0] << endl;

		//row 2
		cout << sides[3][2] << " |";
		for (int i = 4; i < 8; i++)
			cout << tempGrid[i] << " ";
		cout << "| " << sides[1][1] << endl;

		//row 3
		cout << sides[3][1] << " |";
		for (int i = 8; i < 12; i++)
			cout << tempGrid[i] << " ";
		cout << "| " << sides[1][2] << endl;

		//row 4
		cout << sides[3][0] << " |";
		for (int i = 12; i < 16; i++)
			cout << tempGrid[i] << " ";
		cout << "| " << sides[1][3] << endl;

		cout << "   -------" << endl;

		cout << "   ";
		for (int i = 3; i >= 0; i--)
			cout << sides[2][i] << " ";
		cout << endl;
	}

private:
	/**
	 * this function will take the board and try to find a solution using recursion. It will check duplicates on the rows and columns
	 * every time, but will save checking the sides until a complete board is made.
	 * */
	bool recursive_solve()
	{
		//every board space must have a number, so we do not need to loop through each space
		vector<int> tempGrid = grid;
		//check the current space in the grid for each possible number (1-4)
		for (int j = 1; j < BOARDLEN + 1; j++)
		{
			tempGrid[0] = j;
			if (recursive_solve_helper(tempGrid, 1))
				return true;
		}
		return false;
	}

	/**
	 * recursive_solve() 2: electric boogaloo
	 * */
	bool recursive_solve_helper(vector<int> &tempGrid, const int &pos)
	{
		if (tempGrid.size() != (size_t)(BOARDLEN * BOARDLEN) || pos > BOARDLEN * BOARDLEN || pos < 0)
		{
			throw "Invalid argument to recursive_solve_helper";
		}
		//the easiest check to compute, and also the most common error when checking every space for every number
		if (!check_duplicates(tempGrid))
		{
			return false;
		}
		//if the board is 'valid', but is not complete, and all 16 spaces are filled,
		//the board is not valid
		if (pos == BOARDLEN * BOARDLEN)
		{
			if (!check_valid(tempGrid))
				return false;
			//if the board is valid, and all 16 spots are filled, we have the solution
			swap(tempGrid, grid);
			return true;
		}

		//make another grid and test it to see if it will work
		vector<int> tempGrid2 = tempGrid;
		for (int j = 1; j < BOARDLEN + 1; j++)
		{
			tempGrid2[pos] = j;
			if (recursive_solve_helper(tempGrid2, pos + 1))
				return true;
		}

		return false;
	}

	bool check_valid()
	{
		return check_valid(grid);
	}

	/**
	 * This will check the current state of the board to see if it is a valid board.
	 * It will only return false if the board's rules are broken. If the board is incomplete, 
	 * but none of the rules are broken, the result will be true.
	 * */
	bool check_valid(const vector<int> &tempGrid)
	{
		if (tempGrid.size() != (size_t)(BOARDLEN * BOARDLEN))
		{
			throw "Invalid argument to check_valid";
		}
		check_duplicates(tempGrid);

		//checking the left and right sides
		//for each row
		for (int rowpos = 0; rowpos < BOARDLEN; rowpos++)
		{
			//if both rows are not given, don't check it.
			if (sides[3][BOARDLEN - rowpos - 1] == 0 && sides[1][rowpos] == 0)
			{
				continue;
			}
			//copy the row                  beg + 0, beg + 4 |||| beg + 4, beg + 8, etc.
			vector<int> row = vector<int>(tempGrid.begin() + rowpos * BOARDLEN, tempGrid.begin() + (rowpos + 1) * BOARDLEN);
			int count = 0;
			int small = 0;
			//loop over the positions and keep track of which ones count towards the side number
			for (int gridpos = 0; gridpos < BOARDLEN; gridpos++)
			{
				if (row[gridpos] > small)
				{
					small = row[gridpos];
					count++;
				}
			}
			//only checks to see if the row's count is higher than it should be
			if (sides[3][BOARDLEN - rowpos - 1] != count && sides[3][BOARDLEN - rowpos - 1] != 0)
				return false;

			//do the same calculations on the right row
			small = count = 0;
			for (int gridpos = BOARDLEN - 1; gridpos >= 0; gridpos--)
			{
				if (row[gridpos] > small)
				{
					small = row[gridpos];
					count++;
				}
			}
			if (sides[1][rowpos] != count && sides[1][rowpos] != 0)
				return false;
		}

		//check the top and bottom sides
		//for each column
		for (int colpos = 0; colpos < BOARDLEN; colpos++)
		{
			//same as above here
			if (sides[0][colpos] == 0 && sides[2][BOARDLEN - colpos - 1] == 0)
			{
				continue;
			}
			//we can't just copy the row, so instead we will create it via indexing
			vector<int> row;
			for (int gridpos = colpos; gridpos < colpos + BOARDLEN * BOARDLEN; gridpos += BOARDLEN)
				row.push_back(tempGrid[gridpos]);

			int count = 0;
			int small = 0;
			//same deal as for the left and right as for the top
			for (int gridpos = 0; gridpos < BOARDLEN; gridpos++)
			{
				if (row[gridpos] > small)
				{
					small = row[gridpos];
					count++;
				}
			}
			if (sides[0][colpos] != count && sides[0][colpos] != 0)
				return false;

			//and again for the bottom
			small = count = 0;
			for (int gridpos = BOARDLEN - 1; gridpos >= 0; gridpos--)
			{
				if (row[gridpos] > small)
				{
					small = row[gridpos];
					count++;
				}
			}
			if (sides[2][BOARDLEN - colpos - 1] != count && sides[2][BOARDLEN - colpos - 1] != 0)
				return false;
		}
		//if we get here, the board does not have any glaring issues
		return true;
	}

	bool check_duplicates()
	{
		return check_duplicates(grid);
	}

	/**
	 * This function will check to see if there are any non-zero duplicates across the rows and columns
	 * */
	bool check_duplicates(const vector<int> &tempGrid)
	{
		if (tempGrid.size() != (size_t)(BOARDLEN * BOARDLEN))
		{
			throw "Invalid argument to check_duplicates";
		}
		if (tempGrid[0] == tempGrid[1])
		{
			return false;
		}
		//check the rows to make sure there are unique numbers in each space
		//for each row
		for (int rowpos = 0; rowpos < BOARDLEN; rowpos++)
		{
			//set up an array to store if the number has been found
			vector<bool> duplicates(BOARDLEN, false);
			//get the row
			vector<int> row = vector<int>(tempGrid.begin() + rowpos * BOARDLEN, tempGrid.begin() + (rowpos + 1) * BOARDLEN);

			//check each spot in the row to see if it is a duplicate of other numbers in the row
			for (auto val : row)
			{
				if (val == 0)
					continue;

				if (duplicates[val - 1])
					return false;

				duplicates[val - 1] = true;
			}
		}

		//for each column
		for (int colpos = 0; colpos < BOARDLEN; colpos++)
		{
			vector<bool> duplicates(BOARDLEN, false);
			vector<int> row;
			for (int gridpos = colpos; gridpos < colpos + BOARDLEN * BOARDLEN; gridpos += BOARDLEN)
			{
				row.push_back(tempGrid[gridpos]);
			}
			for (auto val : row)
			{
				if (val == 0)
					continue;

				if (duplicates[val - 1])
					return false;

				duplicates[val - 1] = true;
			}
		}

		return true;
	}
};

int main(int argc, const char **argv)
{
	//the file to get the sides from
	string filename;

	//check to see if the file is given by the command line or should be hard-coded
	if (argc < 2)
	{
		filename = "/mnt/c/Users/Adam/Desktop/current/algorithms/assignments/a1/input.txt";
	}
	else
	{
		filename = argv[1];
	}

	string line;
	ifstream file(filename);
	if (!file)
	{
		cout << "The input file: " << filename << "could not be found. " << endl;
		return 1;
	}

	vector<vector<vector<int>>> inputs;

	//we need to keep track of which vector to put in numbers
	int linenum = 0;
	const int BOARDLEN = 4;
	vector<vector<int>> input(BOARDLEN, vector<int>(BOARDLEN, 0));
	while (getline(file, line))
	{
		//This code assumes the input files are given in the 6x6 structure with the sides listed along the edges.
		if (line.length() >= 10)
		{
			//top row
			if (linenum == 0)
			{
				//accounting for spaces in the input files
				for (int i = 0; i < 8; i += 2)
				{
					input[0][i / 2] = line[i + 2] - '0';
					if (input[0][i / 2] > BOARDLEN || input[0][i / 2] < 0)
					{
						cout << "Invalid input was given in the input file. " << endl;
						return 1;
					}
				}
				linenum++;
			}
			//middle rows
			else if (linenum != 5)
			{
				input[1][linenum - 1] = line[10] - '0';
				if (input[1][linenum - 1] > BOARDLEN || input[1][linenum - 1] < 0)
				{
					cout << "Invalid input was given in the input file. " << endl;
					return 1;
				}
				input[3][BOARDLEN - linenum] = line[0] - '0';
				if (input[3][BOARDLEN - linenum] > BOARDLEN || input[3][BOARDLEN - linenum] < 0)
				{
					cout << "Invalid input was given in the input file. " << endl;
					return 1;
				}
				linenum++;
			}
			//bottom row
			else
			{
				for (int i = 0; i < 8; i += 2)
				{
					input[2][i / 2] = line[8 - i] - '0';
					if (input[2][i / 2] > 4 || input[2][i / 2] < 0)
					{
						cout << "Invalid input was given in the input file. " << endl;
						return 1;
					}
				}

				//so the data doesn't get destroyed
				auto temp = vector<vector<int>>(BOARDLEN, vector<int>(BOARDLEN, 0));
				swap(temp, input);
				//and with it processed, put it in to be tested later.
				inputs.push_back(temp);
				linenum = 0;
			}
		}
	}

	file.close();

	//begin testing the inputs for solutions
	for (auto input : inputs)
	{
		try
		{
			Skyscraper s(input);

			if (!s.solve())
			{
				cout << "This board has no correct solution. " << endl;
				s.print_board();
			}
			else
			{
				cout << "The solved board is :" << endl;
				s.print_board();
			}
			cout << endl;
		}
		catch (const char *err)
		{
			cout << err << endl;
			continue;
		}
	}

	return 0;
}