/*
Adam Biewer
Algorithms - Jensen
2/23/2020

This program asks the user for two or more words which will then be added together, with each character being assigned a unique number.
Then all possible solutions to the sum which form a word will be calculated. 
*/

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;

//this tells us how many numbers to test for the words. (all numerical calculations will be done in base 10, but I want to test with only 4 numbers)
const size_t BASE = 10;

struct WordSum
{
    vector<string> words;
    vector<string> dictionary;
    vector<char> uniqueChars;

    WordSum(vector<string> inWords, string filename)
    {
        swap(words, inWords);

        //check to see if more than 10 unique characters exist
        if (!make_unique_chars())
        {
            cout << "There are more than " <<BASE<<" unique characters, a solution is not possible." << endl;
            return;
        }

        //makes the dictionary for the words to be checked against
        generateDictionary(filename);

        //check to see
        for (string word : words)
        {
            if (!check_word(word))
            {
                cout << "The word: " << word << " could not be found in the dictionary." << endl;
                return;
            }
        }

        //start time
        int msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

        find_all_solutions();
        //end time
        int msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

        cout << "Computing this took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;
    }

    /**
     * Generates the uniqueChar vector with all of the unique characters in it. If there are more than 10 unique characters in the passed
     * words, it will return false.
     * */
    bool make_unique_chars()
    {
        for (string word : words)
            for (char c : word)
                if (find_position(uniqueChars, c) == uniqueChars.size())
                    uniqueChars.push_back(c);

        if (uniqueChars.size() > BASE)
            return false;

        return true;
    }

    /**
     * This function will take the file given and try to make it into a vector of strings (dictionary)
     * */
    void generateDictionary(string filename)
    {
        ifstream file(filename);
        if (!file)
        {
            throw "Could not open the dictionary file: " + filename;
        }
        string line;
        while (getline(file, line))
        {
            if (line.back() == '\r')
            {
                line.pop_back();
            }
            dictionary.push_back(line);
        }
        file.close();
    }

    /**
     * This function will take the words given by the user and try to assign numbers to the letters. 
     * It should print out all of the solutions which work.
     * */
    void find_all_solutions()
    {
        cout << "good" << endl;
        for (size_t inti = 0; inti < BASE; inti++)
        {
            vector<char> associations(BASE, ' ');
            recursive_assign_chars_to_numbers(associations, 0, inti);
        }
    }

    /**
     * This function recursively generates all pairings of letters to numbers and tests for solutions. 
     * */
    void recursive_assign_chars_to_numbers(vector<char> associations, size_t charPos, size_t numPos)
    {
        //crash-prevention
        if (numPos >= associations.size())
        {
            return;
        }
        //if the spot is already taken, ignore it
        if (associations[numPos] != ' ')
        {
            return;
        }

        associations[numPos] = uniqueChars[charPos++];
        if (charPos >= uniqueChars.size())
        {
            //we have gone through all of the unique characters for this iteration, so check the sum
            check_sum(associations);
            return;
        }

        for (size_t i = 0; i < associations.size(); i++)
        {
            recursive_assign_chars_to_numbers(associations, charPos, i);
        }

        return;
    }

    /**
     * This function will take the assocations given and compute the sum of the inputs, then pass the sum to find_valid_word
     * */
    void check_sum(vector<char> associations)
    {
        vector<int> wordsAsNumbers;

        int sum = 0;
        int index = 0;
        for (string word : words)
        {
            //if the word would start with a 0, we do not need to count it
            if (find_position(associations, word[0]) == 0)
            {
                return;
            }
            wordsAsNumbers.push_back(0);
            //generate the integer representation of the current word
            int multiplier = 1;
            for (size_t i = word.size(); i > 0; i--)
            {
                int value = (find_position(associations, word[i - 1]));
                wordsAsNumbers[index] += multiplier * value;
                multiplier *= BASE;
            }
            sum += wordsAsNumbers[index];
            index++;
        }

        //now that we have the sum, pass it to find_valid_word to print out all valid words.
        find_valid_words(associations, wordsAsNumbers, sum);
    }

    /**
     * This function will take the sum given and the associations provided and generate all possible solutions to the word puzzle
     * */
    void find_valid_words(vector<char> &associations, vector<int> &wordsAsNumbers, int sum)
    {
        //start by getting the digits of 'sum'
        int temp = sum;
        vector<int> sumVals;
        for (; temp > 0; temp /= BASE)
        {
            sumVals.push_back(temp % BASE);
        }

        //vectors to store the answers to the puzzle
        vector<char> finalWord;

        vector<string> correctWords;
        vector<int> correctSums;

        //finds all the valid words and stores them in correctWords
        recursive_find_valid_words(correctWords, correctSums, finalWord, associations, sumVals, 'a', 0);

        //prevent a nasty crash when there are no correct words
        if (correctSums.size() == 0)
        {
            return;
        }

        //all code to print out the solutions
        cout << "For the input: " << endl;
        for (size_t i = 0; i < words.size(); i++)
        {
            cout << words[i] << "  ---  " << wordsAsNumbers[i] << endl;
        }
        cout << "The sum of these words is: " << correctSums[0] << endl;
        cout << "The possible words are: " << endl;
        size_t i = 0;
        for (; i < correctWords.size(); i++)
        {
            if (correctSums[i] != sum)
                continue;
            cout << correctWords[i] << ", ";
            if (i % BASE == 0)
                cout << endl;
        }
        if (i % BASE != 0)
            cout << endl;
        cout << endl;
    }

    void recursive_find_valid_words(vector<string> &correctWords, vector<int> &correctSums, vector<char> finalWord, vector<char> associations, const vector<int> &sumVals, char initChar, size_t initPos)
    {
        //when the final word has the same size as the integer representation, it is a complete word, so check it to be an actual word
        if (finalWord.size() == sumVals.size())
        {
            //the integer was constructed backwards, so reverse the order here
            reverse(finalWord.begin(), finalWord.end());
            string testWord(finalWord.begin(), finalWord.end());
            if (check_word(testWord))
            {
                //if the testword is a repeat, don't include it
                if (find_position(correctWords, testWord) != correctWords.size())
                {
                    return;
                }
                //if the testword starts with a 0, don't include it
                if (find_position(associations, testWord[0]) == 0)
                {
                    return;
                }
                correctWords.push_back(testWord);
                //if the testword is good, include it and its associated number in the correct piles
                int sum = 0;
                int multiplier = 1;
                for (size_t i = testWord.size(); i > 0; i--)
                {
                    int value = (find_position(associations, testWord[i - 1]));
                    sum += multiplier * value;
                    multiplier *= BASE;
                }
                correctSums.push_back(sum);
            }
            return;
        }

        //going through the solution integer and converting it into every possible word
        for (size_t inti = initPos; inti < sumVals.size(); inti++)
        {
            //if you can find the letter
            if (associations[sumVals[inti]] != ' ')
            {
                finalWord.push_back(associations[sumVals[inti]]);
                recursive_find_valid_words(correctWords, correctSums, finalWord, associations, sumVals, 'a', inti + 1);
            }
            else
            {
                //otherwise, try every letter
                for (char c = initChar; c != 'z' + 1; c++)
                {
                    if (find_position(associations, c) != associations.size())
                    {
                        continue;
                    }
                    associations[sumVals[inti]] = c;
                    recursive_find_valid_words(correctWords, correctSums, finalWord, associations, sumVals, c, inti);
                }
            }
        }
    }

    /**
     * Checks the dictionary provided to see if the testWord is in it.
     * */
    bool check_word(string testWord)
    {
        return binary_search(dictionary.begin(), dictionary.end(), testWord);
    }

    /**
     * This will return the index where the value can be found. If it is not found, it will return inVec.size();
     * */
    template <class T>
    size_t find_position(vector<T> inVec, T value)
    {
        return (size_t)(find(inVec.begin(), inVec.end(), value) - inVec.begin());
    }
};

int main()
{
    string filename = "/home/abiewer/Downloads/Dictionary.txt";
    vector<string> inWords;
    cout << "Please enter the words you would like added together ('q' to quit): " << endl;
    cout << "Next word: ";

    //reading in words from the command prompt
    string line = "";
    while (true)
    {
        getline(cin, line);
        if (line == "q")
        {
            break;
        }
        if (line != "")
        {
            inWords.push_back(line);
        }
        cout << "Next word: ";
    }

    if (inWords.size() < 2)
    {
        cout << "Please enter more than two words. " << endl;
        return 0;
    }
    //try to make solutions from the words given
    try
    {
        WordSum ws2(inWords, filename);
    }
    catch (const char *err)
    {
        cout << err << endl;
        return 1;
    }

    return 0;
}