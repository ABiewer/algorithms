/*
Greatest Common Divisor lab
come up with the values for Bezout's identity:
GCD = A*s + B*t
where everything is an integer, and GCD is the greatest common divisor of A and B

*/
#ifndef GDCCPP
#define GDCCPP

typedef long long ll;

#include "GDC.hpp"

using namespace std;

euclid euclidean_algorithm(ll a, ll b)
{
	euclid retval;

	ll q = a / b;
	ll r = a % b;

	if (r == 0)
	{
		retval.gcd = b;
		retval.s = 0; //0*a
		retval.t = 1; //1*b
	}
	else
	{
		euclid prevret = euclidean_algorithm(b, r);
		retval.gcd = prevret.gcd;
		retval.s = prevret.t;
		retval.t = prevret.s - q * prevret.t;
	}

	return retval;
}

#endif