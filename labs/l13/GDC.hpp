/*
Greatest Common Divisor lab
come up with the values for Bezout's identity:
GCD = A*s + B*t
where everything is an integer, and GCD is the greatest common divisor of A and B

*/
#pragma once

typedef long long ll;

#include <iostream>

using namespace std;

struct euclid
{
	ll s, t, gcd;
};

euclid euclidean_algorithm(ll a, ll b);