/*
Objective: To write a C++ program that can decrypt a message encoded via an
RSA cryptosystem given input p, q, and E. The correct decryption will then be output
to the console window. Verifying uniqueness of this lab will be EASY since you should
also include a digital signature of your name (digital signature described below).

Input File: The input file will contain all numbers separated by spaces or
newlines. All numbers will correspond to a character’s ASCII character value upon
encryption; therefore when decoded, every number should be a value between 0 and 127.
The encoded numbers could be much larger. For example, the input text file could look
like:
7608 2405 1941 1941 11157 3739 15189 10749
3319 10720 2405 2405 16617 18989

Output: For the input file above, suppose you are given p = 181, q = 127, and
E = 13 via the console window. Your program should internally translate the above
input to
71 111 98 98 108 101 100 121
88 103 111 111 107 33
and then externally output the characters associated with those ASCII numbers; in
other words,
Gobbledy-gook!
Your program should ALSO follow up with an encoding of your LAST NAME via
the same key.

Notes:
1. A signature for your authenticity is done just like the encoding process but in
reverse. For example, to encode SARA, I would:
(a) Translate to ASCII numeric values (105 65 104 65) // cast to int
(b) raise each numerical value to the D modulo N. Keeping with p = 181 and
q = 127 above, this would take SARA to 18124 2607 12942 2607.
1
(c) That’s it. Note that we would decode this message by raising all letters to the
E and then modding by N.
This is a signature because anyone can decrypt it with your public key E, revealing
a message. The only way the encryption code could decipher the message is if you
used your private key to encode it, thus verifying your authenticity (typically, you
would encode this with YOUR private key and YOUR modulus, but for the sake
of simplicity, let’s just use one set of keys).
2. You will probably have to write your own power mod function, that computes
powers of numbers and mods as you go. You may also want to review that we’ve
discussed a divide and conquer method for doing this which will give significant
algorithmic savings.
3. Don’t forget to use your Euclidean algorithm program with the ability to write
GCDs as integer linear combinations. That’s going to really come in handy!
4. I will be using primes from our previous lab (Lab 12), so it is probably safest to
use long longs.
*/

#include "GDC.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream>
#include <bitset>
#include <algorithm>

/**
 * Calculates (base^exponent)%mod by multiplying base to itself exponent number of times, each time modding by mod
 * Does not work for values where exponent < 1
 * */
ll powermod(ll base, ll exponent, ll modulus)
{
	//algorithm yoinked from https://www.mtholyoke.edu/courses/quenell/s2003/ma139/js/powermod.html
	if ((base < 1) || (exponent < 0) || (modulus < 1)) {
		return 0;
	}
	ll result = 1;
	while (exponent > 0) {
		if ((exponent % 2) == 1) {
		result = (result * base) % modulus;
		}
		base = (base * base) % modulus;
		exponent = exponent / 2;
	}
	return (result);
}

vector<ll> readInFile(string filename)
{
	ifstream file(filename);
	
	if(!file)
	{
		throw "Could not open file: " + filename;
	}

	vector<ll> retval;

	string line;
	while(getline(file, line))
	{
		try{
			//search for spaces, split string apart based on those
			auto iter = find(line.begin(), line.end(), ' ');
			while(iter!=line.end())
			{
				retval.push_back(stoll(line));

				line = line.substr(iter-line.begin()+1);
				iter = find(line.begin(), line.end(), ' ');
			}
			retval.push_back(stoll(line));
		}
		catch(...) //don't really care about empty lines or bad data
		{
		}
	}

	return retval;
}

int main(int argc, char** argv)
{
	//p and q are two prime numbers which are the private part of the key
	ll p = 181;
	//p = 13;
	ll q = 127;
	//q = 7;


	//E is the public key, a number which is coprime to the product (p-1)(q-1)
	ll E = 13;
	//E = 11;

	//the secret key is a number which fulfills the property:
	// 	E*D % ( (p-1)(q-1) ) == 1
	//		found using the method described below
	ll D;

	//the filename
	string filename = "input.txt";

	//Messages are encoded as : (value)^E % N

	//if you have an encoded message, decoding it is just : (value)^D % N

	//having large values for p, q, and D would make it extremely time consuming, if not cryptographically impossible to determine what
	//those values are before the universe ends (not withstanding the possibilities of quantum computing)

	//using the Euclidean algorithm found in GDC.cpp, the value of D can be found easily as D = s % ( (q-1)(p-1) )
	// 	the arguments for the euclidean algorithm are E and (q-1)(p-1)
	//		since p and q are private, this is a secure process to find D

	//Here is where command line arguments will be processed
	if(argc>4){
		try{
			p = stoll(argv[1]);
			q = stoll(argv[2]);
			E = stoll(argv[3]);
			filename = argv[4];
		}
		catch(...)
		{
			cout<<"Invalid command line arguments. "<<endl;
			return 1;
		}
	}


	//the number everything is modded by is the product of p and q
	ll N = p * q;

	ll pq = (q - 1) * (p - 1);

	euclid ans = euclidean_algorithm(E, pq);

	D = ans.s % pq;

	//of course, D cannot be negative for the algorithm to work, so we make it positive if it is negative by adding (q-1)*(p-1)
	if(D<0)
	{
		D += pq;
	}

	// cout << "E*D%N == " << (E * D) % pq << endl;

	vector<ll> letters;

	try{
		letters = readInFile(filename);
	}
	catch(string err)
	{
		cout<<err<<endl;
		return 1;
	}

	for(size_t i = 0;i<letters.size();i++){
		letters[i] = powermod(letters[i], D, N);
		cout<<(char)(letters[i]);
	}
	cout<<endl;

	vector<ll> nameNums;

	vector<char> name = {'B','i','e','w','e','r'};

	cout<<"Biewer encoded is: ";
	for(size_t i = 0;i<name.size();i++){
		nameNums.push_back(powermod((ll)name[i], E, N));
		cout<<nameNums.back()<<" ";
	}
	cout<<endl;

	// for(size_t i = 0;i<nameNums.size();i++){
	// 	cout<<(char)(powermod(nameNums[i], D, N));
	// }
	// cout<<endl;

	return 0;
}