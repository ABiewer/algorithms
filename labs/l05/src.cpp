/*
Greatest Common Divisor lab
come up with the values for Bezout's identity:
GCD = A*s + B*t
where everything is an integer, and GCD is the greatest common divisor of A and B

*/
typedef long long ll;

#include <iostream>

using namespace std;

struct euclid
{
	ll s, t, gcd;
};

euclid euclidean_algorithm(ll a, ll b);

int main(int argc, char **argv)
{
	ll A = 3084;
	ll B = 1424;

	if (argc > 2)
	{
		try
		{
			A = stoll(argv[1]);
			B = stoll(argv[2]);
		}
		catch (invalid_argument)
		{
			cout << "Invalid arguments: " << argv[1] << ", " << argv[2] << endl;
			return 1;
		}
		catch (out_of_range)
		{
			cout << "Invalid arguments: " << argv[1] << ", " << argv[2] << endl;
			return 1;
		}
	}

	euclid comm = euclidean_algorithm(A, B);
	cout << "The gcd is: " << comm.gcd << endl;

	cout << comm.gcd << " = " << A << "*" << comm.s << " + " << B << "*" << comm.t << endl;
	cout << A * comm.s + B * comm.t << endl;

	return 0;
}

euclid euclidean_algorithm(ll a, ll b)
{
	euclid retval;

	ll q = a / b;
	ll r = a % b;

	if (r == 0)
	{
		retval.gcd = b;
		retval.s = 0; //0*a
		retval.t = 1; //1*b
	}
	else
	{
		euclid prevret = euclidean_algorithm(b, r);
		retval.gcd = prevret.gcd;
		retval.s = prevret.t;
		retval.t = prevret.s - q * prevret.t;
	}

	return retval;
}