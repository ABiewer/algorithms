#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <climits>
#include <algorithm>
#include <iomanip>
#include <chrono>
#include <list>
#include <set>

#define matrix vector<vector<double>>

using namespace std;

struct Matrix
{
private:
	matrix mat;
	vector<vector<size_t>> goodConversions;
	vector<double> rates;

public:
	Matrix(const string& filename);
	Matrix(const matrix& inMatrix);
	void print_good_conversions();

private:
	vector<double> read_row(string line);
	vector<size_t> read_row_size_t(string line);
	void check_matrix();
	void do_conversion_recursion(const size_t& first, vector<size_t> visitedNodes);
	void remove_duplicate_conversions();
};