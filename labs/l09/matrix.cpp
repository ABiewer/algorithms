#include "matrix.hpp"

using namespace std;

Matrix::Matrix(const string &filename)
{
	int msStart, msEnd;
	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	//the first thing to do is to read in the file and convert it to a matrix
	ifstream file(filename);

	//input/error checking
	if (!file)
	{
		cout << "Could not open the file: " << filename << endl;
		return;
	}

	string line;
	//loop every line and get them into a vector of doubles
	while (getline(file, line))
	{
		while (line.back() == '\r' || line.back() == ' ')
			line = line.substr(0, line.size() - 1);
		if (line == "")
			continue;

		vector<double> row = read_row(line);
		mat.push_back(row);
	}

	file.close();
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	cout << "Reading the file took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	check_matrix();

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	for (size_t i = 0; i < mat.size(); i++)
	{
		vector<size_t> visitedNodes{i};
		do_conversion_recursion(i, visitedNodes);
	}
	for (size_t i = 0; i < goodConversions.size(); i++)
	{
		goodConversions[i].pop_back();
	}
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	cout << "Finding conversions took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	remove_duplicate_conversions();
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	cout << "Removing duplicates took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	//sort the list by the number of entries
	size_t count = 0;
	vector<vector<size_t>> tempConversions;
	vector<double> tempRates;
	size_t number = 2;
	while (count < goodConversions.size() && number <= mat.size())
	{
		for (size_t i = 0; i < goodConversions.size(); i++)
		{
			if (goodConversions[i].size() == number)
			{
				tempConversions.push_back(goodConversions[i]);
				tempRates.push_back(rates[i]);
				count++;
			}
		}
		number++;
	}
	swap(goodConversions, tempConversions);
	swap(tempRates, rates);
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	cout << "Sorting took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;
}

Matrix::Matrix(const matrix &inMatrix)
{
	mat = inMatrix;
	check_matrix();
}

vector<double> Matrix::read_row(string line)
{
	vector<double> retval;
	size_t pos;
	while (string::npos != (pos = line.find(' ')))
	{
		retval.push_back(stod(line));
		line = line.substr(pos + 1);
	}
	retval.push_back(stod(line));
	return retval;
}

vector<size_t> Matrix::read_row_size_t(string line)
{
	vector<size_t> retval;
	size_t pos;
	while (string::npos != (pos = line.find(' ')))
	{
		retval.push_back((size_t)(stoi(line)));
		line = line.substr(pos + 1);
	}
	retval.push_back(stod(line));
	return retval;
}

void Matrix::check_matrix()
{
	for (size_t i = 0; i < mat.size(); i++)
		if (mat[i].size() != mat.size())
			throw "The matrix is not square";
}

void Matrix::print_good_conversions()
{
	//print them out here, checking for duplicates

	cout.precision(16);

	size_t i = goodConversions.size()-1000;
	if(i>goodConversions.size())
		i = 0;

	for (; i < goodConversions.size(); i++)
	{
		cout << "Exchange currencies: ";
		for (size_t j = 0; j < goodConversions[i].size(); j++)
		{
			cout << goodConversions[i][j] << " ";
		}
		cout << setw(16 + (mat.size() - goodConversions[i].size()) * 2) << " for a rate of: " << rates[i] << ".\n";
	}
}

void Matrix::do_conversion_recursion(const size_t &first, vector<size_t> visitedNodes)
{
	if (visitedNodes.back() == visitedNodes.front() && visitedNodes.size() > 1)
	{
		double rate = 1.0;

		//calculate the rate of the conversion
		auto last = visitedNodes.begin();
		auto next = visitedNodes.begin();
		next++;
		while (*next != first)
		{
			rate *= mat[*last][*next];
			next++;
			last++;
		}
		rate *= mat[*last][*next];

		if (rate > 1.0)
		{
			rates.push_back(rate);
			goodConversions.push_back(visitedNodes);
		}
		return;
	}

	for (size_t i = 0; i < mat.size(); i++)
	{
		if (i == visitedNodes.front())
		{
			visitedNodes.push_back(i);
			do_conversion_recursion(first, visitedNodes);
			visitedNodes.pop_back();
			continue;
		}
		if (find(visitedNodes.begin(), visitedNodes.end(), i) == visitedNodes.end())
		{
			visitedNodes.push_back(i);
			do_conversion_recursion(first, visitedNodes);
			visitedNodes.pop_back();
		}
	}
}

void Matrix::remove_duplicate_conversions()
{
	//rotate every vector in goodConversions so that the minimum element is the first element
	for (size_t i = 0; i < goodConversions.size(); i++)
	{
		vector<size_t> &currVec = goodConversions[i];
		size_t minelement = 0;
		for (size_t j = 0; j < currVec.size(); j++)
		{
			if (currVec[j] < currVec[minelement])
				minelement = j;
		}
		rotate(currVec.begin(), currVec.begin() + minelement, currVec.end());
	}

	//sort the vectors based on size. If the algorithm below finds that two vectors are not the same size, it can stop searching on that vector
	std::sort(goodConversions.begin(), goodConversions.end(), [](const vector<size_t> &a, const vector<size_t> &b) { return a.size() < b.size(); });

	cout << "elements: " << goodConversions.size() << endl;

	set<pair<string, double>> tempConversions;

	for (size_t i = 0; i < goodConversions.size(); i++)
	{
		string left = "";
		for (size_t j = 0; j < goodConversions[i].size(); j++)
		{
			left += to_string(goodConversions[i][j]) + " ";
		}
		left.pop_back();
		tempConversions.insert(pair<string, double>(left, rates[i]));
	}

	goodConversions.clear();
	rates.clear();
	for (auto iter = tempConversions.begin(); iter != tempConversions.end(); iter++)
	{
		string left = iter->first;
		double rate = iter->second;
		vector<size_t> row = read_row_size_t(left);
		goodConversions.push_back(row);
		rates.push_back(rate);
	}
}