/*
A lab to find profitable conversion rates between currencies

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>

#include "matrix.hpp"

using namespace std;

int main(int argc, char** argv)
{
	string filename;
	if(argc>1)
	{
		filename = argv[1];
	}
	else{
		filename = "/mnt/c/Users/Adam/Desktop/current/algorithms/labs/l9/testfiles/sixrows.txt";
	}

	try{
		streamsize ss = cout.precision();
		int msStart, msEnd;

		msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

		Matrix m1(filename);

		msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

		m1.print_good_conversions();

		cout.precision(ss);
		cout << "Computing the conversions took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;
	}
	catch(const char ** err)
	{
		cout<<err<<endl;
		return 1;
	}
	catch(invalid_argument){
		cout<<"There is an invalid number in the matrix. "<<endl;
		return 1;
	}
	catch(out_of_range){
		cout<<"One of the numbers in the matrix is too large to be processed. "<<endl;
		return 1;
	}
	catch(string err){
		cout<<err<<endl;
		return 1;
	}

	return 0;
}