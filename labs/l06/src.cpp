/*
Author: Adam Biewer
Class: Algorithms, Lab 6

The freelancer lab

The goal of the lab is to take a set of jobs and pay rates associated with those jobs and figure out which
ones a freelancer should take to maximize his profits in a given week. 

No two jobs may be due on the same day. 
All jobs for that week must be selected at the same time, at the start of the week.

The output should assume two different scenarios:
1. the freelancer is able to multi-task, and will get all of the jobs done, assuming they are all due on different days.
2. The freelancer is not able to multi-task, and will complete them sequentially

The output will be the amount of money earned that week, as well as the indiviudal jobs completed for both scenarios. */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <vector>
#include <list>
#include <chrono>

using namespace std;

void findSubsetsRecursiveHelper(vector<vector<size_t>> &jobsSubset, const size_t &curr, const size_t &size, const vector<int> &days);

void insertElement(list<int> &indices, list<int> &dollars, const size_t &index, const int &dollar)
{
	auto iter = indices.begin();
	auto diter = dollars.begin();
	for (; iter != indices.end();)
	{
		if (dollar > *diter)
		{
			dollars.insert(diter, dollar);
			indices.insert(iter, index);
			return;
		}
		iter++;
		diter++;
	}
	indices.push_back(index);
	dollars.push_back(dollar);
	return;
}

void trimList(vector<int> &days, vector<int> &dollars, vector<int> &jobNums)
{
	//going through the days and making a new vector of days which only include the largest values from the given values
	//there will be 7 jobs that take 1 day, 6 for 2, ... 1 for 7
	vector<int> retvalDays, retvalDollars, retvalJobNums;
	for (int dayNum = 1; dayNum < 8; dayNum++)
	{
		size_t countMax = 8 - dayNum;
		list<int> topIndices, topDollars;
		for (size_t i = 0; i < days.size(); i++)
		{
			if (days[i] == dayNum)
			{
				if (topDollars.size() < countMax)
				{
					insertElement(topIndices, topDollars, i, dollars[i]);
				}
				else if (topDollars.back() < dollars[i])
				{
					insertElement(topIndices, topDollars, i, dollars[i]);
					topIndices.pop_back();
					topDollars.pop_back();
				}
			}
		}
		auto iter = topIndices.begin();
		auto diter = topDollars.begin();
		for (; iter != topIndices.end();)
		{
			retvalJobNums.push_back(*iter + 1);
			retvalDays.push_back(days[*iter]);
			retvalDollars.push_back(*diter);
			iter++;
			diter++;
		}
	}
	jobNums = retvalJobNums;
	days = retvalDays;
	dollars = retvalDollars;
	return;
}

bool isValid(vector<int> &days)
{
	int sum = 0;
	for (size_t i = 0; i < days.size(); i++)
	{
		sum += days[i];
	}
	if (sum > 7)
		return false;
	return true;
}

vector<vector<size_t>> findSubsets(const size_t &size, const vector<int> &days)
{
	vector<vector<size_t>> jobsSubset;
	for (size_t i = 0; i < size; i++)
	{
		vector<size_t> sub{i}; //a vector with contents [i]
		jobsSubset.push_back(sub);
		if (days[i] < 7)
			findSubsetsRecursiveHelper(jobsSubset, jobsSubset.size() - 1, size, days);
	}

	return jobsSubset;
}

void findSubsetsRecursiveHelper(vector<vector<size_t>> &jobsSubset, const size_t &curr, const size_t &size, const vector<int> &days)
{
	//the maximum number of jobs that can be taken on is 7 in a week
	if (jobsSubset[curr].size() > 6 || jobsSubset[curr].back() == size)
	{
		return;
	}

	for (size_t i = jobsSubset[curr].back() + 1; i < size; i++)
	{
		vector<size_t> sub = jobsSubset[curr];

		sub.push_back(i);
		//check to see if the job's day limit exceeds the 7 day week
		vector<int> subsetDays;
		for (size_t j = 0; j < sub.size(); j++)
		{
			subsetDays.push_back(days[sub[j]]);
		}

		if (isValid(subsetDays))
		{
			jobsSubset.push_back(sub);
			findSubsetsRecursiveHelper(jobsSubset, jobsSubset.size() - 1, size, days);
		}
	}
}

/**
 * Finds the optimal jobs the freelancer should take assuming that they cannot multitask
 * */
vector<size_t> findOptimalJobs(const vector<int> &days, const vector<int> &dollars)
{
	//make two vectors to hold the valid day and dollar assignments
	vector<vector<int>> validDays, validDollars;
	vector<size_t> validSubsets;

	//start by getting all possible 1-7 day subsets of jobs that can be completed
	vector<vector<size_t>> subsets = findSubsets(days.size(), days);

	//for each subset, check to see if the jobs listed in that subset can actually be completed
	for (size_t i = 0; i < subsets.size(); i++)
	{
		//hold the subset we are currently investigating
		vector<size_t> subset = subsets[i];

		//make a vector to hold the day amounts related to that job
		vector<int> subsetDays;

		//fill the day amounts with the correct numbers
		for (size_t j = 0; j < subset.size(); j++)
		{
			size_t dayIndex = subset[j];
			subsetDays.push_back(days[dayIndex]);
		}

		if (isValid(subsetDays))
		{
			validSubsets.push_back(i);

			validDays.push_back(subsetDays);

			vector<int> subsetDollars;
			for (size_t j = 0; j < subset.size(); j++)
			{
				size_t dollarIndex = subset[j];
				subsetDollars.push_back(dollars[dollarIndex]);
			}

			validDollars.push_back(subsetDollars);
		}
	}

	//now select the best option from all of the valid options.
	int bestChoice = 0;
	int maxDollars = 0;
	for (size_t i = 0; i < validDollars.size(); i++)
	{
		int sum = 0;
		for (size_t j = 0; j < validDollars[i].size(); j++)
		{
			sum += validDollars[i][j];
		}
		if (sum > maxDollars)
		{
			maxDollars = sum;
			bestChoice = i;
		}
	}

	return subsets[validSubsets[bestChoice]];
}

vector<size_t> findOptimalJobsMultiTask(vector<int> &days, vector<int> &dollars)
{
	/*
	Author: Adam Biewer
	Class: Algorithms, Lab 6
	*/
	vector<size_t> takenJobs;
	for (int day = 1; day < 8; day++)
	{
		int maxDollar = 0;
		size_t index = 0;
		for (size_t i = 0; i < dollars.size(); i++)
		{
			if (dollars[i] > maxDollar)
			{
				if (days[i] <= day && find(takenJobs.begin(), takenJobs.end(), i) == takenJobs.end())
				{
					maxDollar = dollars[i];
					index = i;
				}
			}
		}
		takenJobs.push_back(index);
	}

	return takenJobs;
}

int main(int argc, char **argv)
{
	//the job number is tracked by the index
	vector<int> jobNums;
	vector<int> days;
	vector<int> dollars;

	string filename;

	if (argc > 1)
	{
		filename = argv[1];
	}
	else
	{
		filename = "/home/abiewer/Documents/current/algorithms/labs/l6/testinput.txt";
	}

	fstream file(filename);

	if (file)
	{
		string line;
		while (getline(file, line))
		{
			if (line == "")
				continue;

			jobNums.push_back(stoi(line));

			size_t pos = line.find(' ');
			line = line.substr(pos + 1); //1 past space
			days.push_back(stoi(line));

			pos = line.find(' ');
			line = line.substr(pos + 2); //1 past space and 1 past $
			dollars.push_back(stoi(line));
		}

		file.close();
	}
	else
	{
		cout << "The file: " << filename << " could not be opened. " << endl;
		return 1;
	}

	int msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	vector<size_t> optimalMulti = findOptimalJobsMultiTask(days, dollars);
	int msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	trimList(days, dollars, jobNums);

	cout << "Optimal multitasking jobs: " << endl;
	cout << "Job Number | Job Days | Job Pay" << endl;
	for (size_t i = 0; i < optimalMulti.size(); i++)
	{
		cout << setw(10) << jobNums[optimalMulti[i]] << "   " << setw(8) << days[optimalMulti[i]] << "   $" << dollars[optimalMulti[i]] << endl;
	}
	int sum = 0;
	for(int i = 0;i<optimalMulti.size();i++){
		sum+=dollars[optimalMulti[i]];
	}
	cout<<"For a total of $"<<sum<<" this week."<<endl;
	cout << "Computing this took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	cout<<endl;

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	vector<size_t> optimalSingle = findOptimalJobs(days, dollars);
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout << "Optimal non-multitasking jobs: " << endl;
	cout << "Job Number | Job Days | Job Pay" << endl;
	for (size_t i = 0; i < optimalSingle.size(); i++)
	{
		cout << setw(10) << jobNums[optimalSingle[i]] << "   " << setw(8) << days[optimalSingle[i]] << "   $" << dollars[optimalSingle[i]] << endl;
	}
	sum = 0;
	for (int i = 0; i < optimalSingle.size(); i++)
	{
		sum += dollars[optimalSingle[i]];
	}
	cout << "For a total of $" << sum << " this week." << endl;
	cout << "Computing this took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	//vector<vector<size_t>> subsets = findSubsets(4);

	return 0;
}