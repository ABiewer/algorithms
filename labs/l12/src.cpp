/* 
Objective: To write a C++ program that:
1. Implements a Sieve of Eratosthenes to determine all prime numbers up to MAX =
1073741823. Your program should then output these primes to a file, one prime on
each line. DONE
2. Can open a file of prime numbers separated by newline commands and read the
primes back in, storing them in a vector of (unsigned) integers.
3. Can determine whether or not 4,153,748,658,054,516,049 and 29,996,224,275,833
are prime.
The goal of this lab is to run a few experiments on timing to learn just how long it
is with these small examples to determine whether or not numbers are prime, and to
possibly learn some cool things about C++ I didn’t know before this lab.
You will run the lab twice, once in debug mode and once in release mode. You should
submit snaps or screenshots of your final outputs with timing. Note that I don’t really
care how long it takes your computer to write the output file, so you actually probably
want to run the program 3 times; once to create the output file, once to time in debug,
and once to time in release. An example with INCORRECT timing numbers is shown
below. You should also change the word A LOT below to the number of primes in the
file.


The Sieve of Eratosthenes took 100 seconds in debug mode.
Reading the primes in from the file took 200 seconds in debug mode.
There are A LOT of primes.
The number 8675309 is prime. Determining this took 5 seconds in debug mode.
The number 348669067 is not prime. Determining this took 1 seconds in debug mode.


The Sieve of Eratosthenes took 10 seconds in release mode.
Reading the primes in from the file took 20 seconds in release mode.
The number 8675309 is prime. Determining this took 0 seconds in release mode.
The number 348669067 is not prime. Determining this took 0 seconds in release mode.


Input: There is no input file for this program.
Output: Output should be as described above.
Notes: I learned a lot of things while coding this lab! You may very well know them,
but in case you don’t, here are my takeaways.

1. the internal maxes of datatypes CAN change over time, but my current computer
has an int max of 2,147,483,647. The value I’ve given you for checking primes is
half of this. Also, the long long max is the square of the int max.
2. Apparently vectors and bools don’t mix! I had to use arrays or my program would
crash.
3. I also experienced stack overflow issues attempting to create an array holding more
than 10,000,000, but this was resolved by making the array static.
*/

#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <limits.h>

typedef unsigned long long ll;

using namespace std;

const uint MAX = 1073741823; // 0x 4000 0000 - 1

void EratosthenesSieve()
{
	//check to see if we've already made the file.
	ifstream testfile("primes.txt");
	if (testfile)
	{
		testfile.close();
		cout<<"The primes file already exists. "<<endl;
		return;
	}

	int msStart, msEnd;

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	ofstream file;
	file.open("primes.txt");

	//it's easier to program it so that prime numbers will appear false, and then to set all multiples to true
	static bool primes[MAX + 1] = {false};

	// 0x 8000 == sqrt(MAX)
	//any primes above 32768 will not have multiples in the array
	//go from number 2 (i==1) up to sqrt(max) to find which numbers are still prime. If it is prime, set all later multiples of it to false
	for (uint i = 2; i < 0x8001; i++)
	{
		if (!primes[i])
		{
			file << i << endl;
			uint j = i + i;
			for (; j < MAX + 1; j += i)
			{
				primes[j] = true;
			}
		}
	}

	for (uint i = 0x8001; i < MAX + 1; i++)
	{
		if (!primes[i])
			file << i << endl;
	}

	file.close();

	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout << "Computing the Sieve took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;
}

void readInPrimes(vector<uint> &primes)
{

	ifstream file("primes.txt");
	if (!file)
		return;

	int msStart, msEnd;

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	string line;
	uint counter = 0;
	while (getline(file, line))
	{
		try
		{
			primes.push_back((uint)stoul(line));
			counter++;
		}
		catch (...)
		{
			//if the line is empty, we don't care about it( should only happen with the last line if at all)
		}
	}

	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout << "Reading the primes took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	cout << "There are " << counter << " primes. " << endl;
}

void printIsPrime(ll prime, const vector<uint> &primes)
{

	int msStart, msEnd;

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	if (prime < MAX)
	{
		if (binary_search(primes.begin(), primes.end(), prime))
		{
			cout << prime << " is prime. ";
		}
		else
		{
			cout << prime << " is not prime. ";
		}
	}
	else
	{
		auto iter = primes.begin();
		bool isPrime = true;
		while(*iter<0x100000000 && iter!= primes.end())
		{
			if(prime%(*iter) == 0)
			{
				cout<<prime<<" is not prime. ";
				isPrime = false;
				break;
			}
			iter++;
		}
		if(isPrime)
		{
			cout<<prime<<" is prime. ";
		}
	}

	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout << "Computing this took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;
}

int main()
{
	EratosthenesSieve();

	vector<uint> primes;

	readInPrimes(primes);

	printIsPrime((ll)8675309, primes);
	printIsPrime((ll)348669067, primes);
	printIsPrime((ll)29996224275833, primes);
	printIsPrime((ll)4153748658054516049, primes);

	return 0;
}