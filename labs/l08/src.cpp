/*
Muggle Money Lab
In this lab, we are to take the amount of change requested and give back the correct amount in coins using pre-1970's British coins
pence - 1
2 pence - 2
3 pence - 3
6 pence - 6
shilling - 12
florin - 24
half-crown - 30
*/

#include <iostream>
#include <chrono>

using namespace std;

int main(){
	while(true){
		string line;
		cout<<"Please enter an amount to convert to change (q to quit): ";
		getline(cin, line);
		if(line=="q"){
			break;
		}
		int change;
		try{
			change = stoi(line);
		}
		catch(invalid_argument){
			cout<<"Invalid argument, please try again. "<<endl;
			continue;
		}
		catch(out_of_range){
			cout<<"Invalid argument, please try again. "<<endl;
			continue;
		}
		int p1, p2, p3, p6, sh, fl, hc;
		p1=p2=p3=p6=sh=fl=hc=0;

		hc = change/30;
		change %= 30;
		fl = change/24;
		change %= 24;
		sh = change/12;
		change %= 12;
		p6 = change/6;
		change %= 6;
		p3 = change/3;
		change %= 3;
		p2 = change/2;
		change %= 2;
		p1 = change;

		if(hc>0)
			cout<<hc<<" half-crowns, ";
		if(fl>0)
			cout<<"a florin, ";
		if(sh>0)
			cout<<"a shilling, ";
		if(p6>0)
			cout<<"a 6 pence, ";
		if(p3>0)
			cout<<"a 3 pence, ";
		if(p2>0)
			cout<<"a 2 pence, ";
		if(p1>0)
			cout<<"a 1 pence, ";
		cout<<endl<<endl;
	}
	return 0;
}