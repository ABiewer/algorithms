#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <chrono>

typedef unsigned long long ll;

using namespace std;

struct NCR{
	NCR(string filename);
	ll ncr(const ll& n, const ll& r);
	ll getCurrentNumber();
	void increment();
	ll fact(const ll& n);
	void generateFactorials();
	ll straightFactorial(const ll& n);

private:
	size_t nn;
	ll modulo;
	vector<ll> inverses;
	vector<ll> f_n_1;
	vector<ll> factors;

};