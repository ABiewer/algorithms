/*
A combinatorics lab, to find the number of combinations of bags possible in a pantry. each day a single bag is introduced, and 
that bag can take any even number of previously existing bags from the pantry and store them inside the new bag (so that there is only 1 bag
from all of the combined bags. )

*/

#include "ncr.hpp"
#include <iostream>
#include <fstream>
#include <vector>

typedef unsigned long long ll;

using namespace std;

int main(int argc, char **argv)
{
	//( n! * k_inverse * (n-k)_inverse ) % modulo is the correct number

	string filename = "BagsInverses.csv";

	try{
		NCR n1 = NCR(filename);
	}
	catch(const char* err)
	{
		cout<<err<<endl;
		return 1;
	}



	return 0;
}
