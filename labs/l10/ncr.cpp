#include "ncr.hpp"

using namespace std;

ll NCR::straightFactorial(const ll& n)
{
	ll factor = 1;
	for(ll i = 2;i<=n;i++){
		factor = (factor * i)%modulo;
	}
	return factor;
}

inline ll NCR::fact(const ll& n)
{
	return factors[n-1];
}

size_t enterDayNumber()
{
	string line;
	cout << "Please enter the day (1-24680) you would like to know the number of combinations for: ";
	getline(cin, line);

	size_t day;
	try
	{
		day = stoul(line);
	}
	catch (invalid_argument)
	{
		//cout<<"Invalid input. "<<endl;
		return 0;
	}
	catch (out_of_range)
	{
		//cout<<"Input is too large. "<<endl;
		return 0;
	}
	if (day > 24680)
	{
		//cout<<"The day entered is too large to process. "<<endl;
		return 0;
	}

	return day;
}

NCR::NCR(string filename)
{
	generateFactorials();

	ifstream file(filename);

	modulo = 1020202009;
	inverses.reserve(24680);

	if (file)
	{
		string line;
		while (getline(file, line))
		{
			inverses.push_back(stoull(line));
		}
	}
	else
	{
		throw "Could not open the file.";
	}

	size_t day = enterDayNumber();

	if (day == 0)
	{
		throw "Day number entered was invalid. ";
	}

	if(day==1 || day==2)
	{
		cout<<"There is only 1 combination for that day. "<<endl;
		return;
	}

	//on day three, there are 2 possible combinations of bags, which will be the two terms in the equation. 
	nn = 4;
	f_n_1.push_back(1);
	f_n_1.push_back(1);

	int msStart, msEnd;

	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	for (; nn <= day; nn++)
	{
		if (nn % 100 == 1)
		{
			msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
			cout << "Iteration: " << nn-1;
			cout << "   Program time: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;
		}
		increment();
	}

	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout<<"For day: "<<nn-1<<" there are: "<<getCurrentNumber()<<" combinations possible (mod 1020202009)."<<endl;

	cout << "Computing the iterations took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;
	return;
}

void NCR::increment()
{
	vector<ll> f_n;

	//f_n_0 is always 1
	f_n.push_back(1);

	size_t loopMax = (nn-1)/2;

	// a loop for each term in the equation
	for (size_t x = 1; x <= loopMax; x++)
	{
		ll f_n_x = 0;
		//a loop for each term within a term
		for (size_t i = 0; i <= x; i++)
		{
			ll topNum = nn-1-2*i;
			if(topNum==0)
				break;
			ll botNum = 2*x - 2*i;
			f_n_x += f_n_1[i] * ncr(topNum, botNum);
		}
		f_n.push_back(f_n_x % modulo);
	}

	swap(f_n, f_n_1);
}

ll NCR::getCurrentNumber()
{
	ll sum = 0;
	for (auto i : f_n_1)
	{
		sum += i;
	}
	return sum % modulo;
}

ll NCR::ncr(const ll& n, const ll& r)
{
	if(r==0 || r==n)
		return 1;
	ll top = fact(n);
	ll bottom = (inverses[n-r-1] * inverses[r-1])%modulo;
	return (top * bottom) % modulo;
}



void NCR::generateFactorials()
{
	ifstream file("factorials.txt");
	if(file){
		string line;
		while(getline(file, line))
		{
			factors.push_back(stoull(line));
		}
		file.close();
		return;
	}
	
	ofstream outFile("factorials.txt");
	for(int i = 1;i<24680;i++)
	{
		outFile<<straightFactorial(i)<<"\n";
	}

	outFile.close();

	file.open("factorials.txt");
	string line;
	while(getline(file, line))
	{
		factors.push_back(stoull(line));
	}
	file.close();
	return;
}