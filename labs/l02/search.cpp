/**
 * The Input File Search Lab
 * The goal is to take two input files and determine how many words from each are in the other file. 
 * Each word is unique (e.g. multiple "I" in a paragraph are counted multiple times). 
 * 
 * 
 * 
 * */
#include <iostream>
#include <time.h>
#include <chrono>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Takes in a string representing the words of the file and outputs a vector of words from the string. 
 * @param content The contents of the file, is a copy of the string, so I can manipulate it without messing it up outside the function. 
 * @return The vector of words without quotes, spaces, punctuation, etc. 
 * */

/**
 * Code taken from insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
 * Minor adjustments made by myself, including comments
 * 
 * This function returns the contents of a file as a string
 * */
string get_file_contents(string filename)
{
    //input stream
    ifstream in(filename, ios::in | ios::binary);
    //if the input opened the file correctly,
    if (in)
    {
        string contents;
        //seek to the end to get the correct size
        in.seekg(0, ios::end);
        contents.resize(in.tellg());
        //back to the beginning
        in.seekg(0, ios::beg);
        //use the C function to copy all of the data
        in.read(&contents[0], contents.size());
        in.close();
        // and return the contents
        return (contents);
    }
    throw("Could not open the file: " + filename + ". ");
}

/**
 * Will take the string produced from reading the files and remove punctuation, as well as make all the characters lowercase
 * 
 * 
 * */
void filter_out_punctuation(string &fileContents)
{
    //loop through all the characters in the string
    auto iter = fileContents.begin();
    for (; iter != fileContents.end();)
    {
        //if the character is not in the alphabet,
        if (!isalpha(*iter) && *iter != ' ')
        {
            //check if it's an accidental newline
            if (*iter == '\\' && *(iter + 1) == 'n')
            {
                fileContents.insert(iter, ' ');
                fileContents.erase(iter+1);
                iter++;
            }
            if(*iter=='\n'){
                fileContents.insert(iter,' ');
                iter++;
            }
            //or just erase it
            fileContents.erase(iter);
            continue;
        }
        *iter = tolower(*iter);
        iter++;
    }
}

/**
 * This function takes the string of words and breaks it up on spaces to return a vector of the words contained in that string. 
 * 
 * */
vector<string> get_words(string& content)
{
    vector<string> retval;

    size_t index = content.find(' ');

    while (index != string::npos)
    {
        retval.push_back(content.substr(0, index));
        content = content.substr(index + 1);
        index = content.find(' ');
    }

    retval.push_back(content);

    return retval;
}

/**
 * This function takes the words from f1 and checks to see if they are in f2
 * 
 * */
int compare_words(vector<string>& file1, vector<string>& file2)
{
    int retval = 0;
    for (auto iter = file1.begin(); iter != file1.end(); iter++)
    {
        if(*iter==""){
            continue;
        }
        if (find(file2.begin(), file2.end(), *iter) != file2.end())
        {
            retval++;
            continue;
        }
    }
    return retval;
}

/**
 * 
 * 
 * */
void run_on_files(string& f1, string& f2)
{

    //the files that will be compared:
    string &permFileName1 = f1;
    string &permFileName2 = f2;

    cout << "The first file: " << permFileName1 << endl;
    cout << "The second file: " << permFileName2 << endl;

    //start the timer
    int msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

    //getting the words out of the files and into vectors
    string fileContents;
    try
    {
        fileContents = get_file_contents(permFileName1);
    }
    catch (string err)
    {
        cout << err << endl;
        return;
    }
    filter_out_punctuation(fileContents);
    vector<string> file1 = get_words(fileContents);

    fileContents = get_file_contents(permFileName2);
    try
    {
        filter_out_punctuation(fileContents);
    }
    catch (string err)
    {
        cout << err << endl;
        return;
    }
    vector<string> file2 = get_words(fileContents);

    //comparing the vectors

    int f1wordsInf2, f2wordsinf1 = 0;

    f1wordsInf2 = compare_words(file1, file2);
    f2wordsinf1 = compare_words(file2, file1);

	//end the timer
	int msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

	cout << f1wordsInf2 << " words from the first file are contained in the second file. " << endl;
    cout << f2wordsinf1 << " words from the second file are contained in the first file. " << endl;
    cout << "Computing this took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

    return;
}

int main()
{
    //
    string filename1 = "/home/abiewer/Documents/current/algorithms/labs/l2/FILE1.txt";
    string filename2 = "/home/abiewer/Documents/current/algorithms/labs/l2/FILE2.txt";
    string filename3 = "/home/abiewer/Documents/current/algorithms/labs/l2/3PIGS.txt";
    string filename4 = "/home/abiewer/Documents/current/algorithms/labs/l2/3WISHES.txt";
    string filename5 = "/home/abiewer/Documents/current/algorithms/labs/l2/BILLYBUDD.txt";
    string filename6 = "/home/abiewer/Documents/current/algorithms/labs/l2/BLUECRANE.txt";
    string filename7 = "/home/abiewer/Documents/current/algorithms/labs/l2/COMMUNISM.txt";
    string filename8 = "/home/abiewer/Documents/current/algorithms/labs/l2/KINGJAMES.txt";
    string filename9 = "/home/abiewer/Documents/current/algorithms/labs/l2/test1.txt";
    string filename10 = "/home/abiewer/Documents/current/algorithms/labs/l2/test2.txt";

    run_on_files(filename1, filename2);
    run_on_files(filename3, filename4);
    run_on_files(filename5, filename6);
    run_on_files(filename7, filename7);
    run_on_files(filename8, filename8);

    return 0;
}