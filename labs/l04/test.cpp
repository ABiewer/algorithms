/* C++ implementation of QuickSort */
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

/**
 * This function takes last element as pivot, places  
 * the pivot element at its correct position in sorted  
 * array, and places all smaller (smaller than pivot)  
 * to left of pivot and all greater elements to right  
 * of pivot 
 * */
template <typename T>
typename vector<T>::iterator partition(vector<T>& arr, typename vector<T>::iterator& low, typename vector<T>::iterator& high)
{
	T pivot = *high; // pivot
	typename vector<T>::iterator iter = (low - 1);	 // Index of smaller element

	for (typename vector<T>::iterator jter = low; jter != high; jter++)
	{
		// If current element is smaller than the pivot
		if (*jter < pivot)
		{
			iter++; // increment index of smaller element
			swap(*iter, *jter);
		}
	}
	iter++;
	swap(*iter, *high);
	return iter;
}

/** 
 * The main function that implements QuickSort  
 * arr[] --> Array to be sorted
 * low --> Starting iterator
 * high --> Ending iterator 
 * */
template <typename T>
void quickSort(vector<T>& arr, typename vector<T>::iterator low, typename vector<T>::iterator high)
{
	if (low < high)
	{
		// pi is partitioning index, arr[p] is now at right place 
		typename vector<T>::iterator pi = partition(arr, low, high);

		// Separately sort elements before partition and after partition
		quickSort(arr, low, pi - 1);
		quickSort(arr, pi + 1, high);
	}
}

/* Function to print an array */
template <typename T>
void printArray(vector<T>& arr)
{
	size_t i;
	for (i = 0; i < arr.size(); i++)
		cout << arr[i] << " ";
	cout << endl;
}

// Driver Code
int main()
{
	vector<int> arr = {10, 7, 8, 9, 1, 5};
	quickSort(arr, arr.begin(), arr.end()-1);
	cout << "Sorted array: \n";
	printArray(arr);
	return 0;
}