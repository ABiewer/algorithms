/*
Lab to time the sorting efficiency of different algorithms ( bubble, quick, etc. )
*/


#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <string>

using namespace std;

template <typename T>
vector<T> split_merge(vector<T> inVec);

template <typename T>
void recursive_quick_sort(const vector<T> &inVec, typename vector<T>::iterator least, typename vector<T>::iterator pivot);

/**
 * Code taken from insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
 * Minor adjustments made by myself, including comments
 * 
 * This function returns the contents of a file as a string
 * */
string get_file_contents(string filename)
{
	//input stream
	ifstream in(filename, ios::in | ios::binary);
	//if the input opened the file correctly,
	if (in)
	{
		string contents;
		//seek to the end to get the correct size
		in.seekg(0, ios::end);
		contents.resize(in.tellg());
		//back to the beginning
		in.seekg(0, ios::beg);
		//use the C function to copy all of the data
		in.read(&contents[0], contents.size());
		in.close();
		// and return the contents
		return (contents);
	}
	throw("Could not open the file: " + filename + ". ");
}

/**
 * Will take the string produced from reading the files and remove punctuation, as well as make all the characters lowercase
 * 
 * 
 * */
void filter_out_punctuation(string &fileContents)
{
	//loop through all the characters in the string
	string::iterator iter = fileContents.begin();
	for (; iter != fileContents.end();)
	{
		//if the character is not in the alphabet,
		if (!isalpha(*iter) && *iter != ' ')
		{
			//check if it's an accidental newline
			if (*iter == '\\' && *(iter + 1) == 'n')
			{
				fileContents.insert(iter, ' ');
				fileContents.erase(iter + 1);
				iter++;
			}
			/*if(*iter == '\r' && *(iter+1)=='\n'){ //if the characters would be \r\n, the next if statement will take care of the \n
				fileContents.insert(iter, ' ');
				fileContents.erase(iter+1);
				iter++;
			}*/
			if (*iter == '\n')
			{
				fileContents.insert(iter, ' ');
				iter++;
			}
			//or just erase it
			fileContents.erase(iter);
			continue;
		}
		*iter = tolower(*iter);
		iter++;
	}
}

/**
 * This function takes the string of words and breaks it up on spaces to return a vector of the words contained in that string. 
 * 
 * */
vector<string> get_words(string &content)
{
	vector<string> retval;

	size_t index = content.find(' ');

	while (index != string::npos)
	{
		if (index == 0)
		{
			content = content.substr(index + 1);
			index = content.find(' ');
			continue;
		}
		retval.push_back(content.substr(0, index));
		content = content.substr(index + 1);
		index = content.find(' ');
	}

	retval.push_back(content);

	return retval;
}

/**
 * Takes the filename and puts the words from it into a vector of strings in the order they appear in the file.
 * */
vector<string> put_file_contents_into_vector(string filename)
{
	//getting the words out of the files and into vectors
	string fileContents;
	try
	{
		fileContents = get_file_contents(filename);
	}
	catch (string err)
	{
		cout << err << endl;
		return vector<string>();
	}
	filter_out_punctuation(fileContents);
	return get_words(fileContents);
}

/**
 * Will return a sorted vector from the given vector using a bubble sort algorithm
 * */
template <typename T>
vector<T> my_bubble_sort(vector<T> inVec)
{
	vector<T> &retval = inVec;
	for (size_t i = 0; i < retval.size(); i++)
	{
		for (size_t j = i + 1; j < retval.size(); j++)
		{
			if (retval[j] < retval[i])
			{
				swap(retval[j], retval[i]);
			}
		}
	}
	return retval;
}

/**
 * Will return a sorted vector from the given vector using a merge sort algorithm
 * */
template <typename T>
vector<T> my_merge_sort(vector<T> inVec)
{
	vector<T> &retval = inVec;

	retval = split_merge(inVec);

	return retval;
}

/**
 * 
 * */
template <typename T>
vector<T> split_merge(vector<T> inVec)
{
	if (inVec.size() == 1)
	{
		return inVec;
	}
	if (inVec.size() == 2)
	{
		if (inVec[0] > inVec[1])
		{
			swap(inVec[0], inVec[1]);
		}
		return inVec;
	}
	//inVec.size() >= 3

	//find the midpoint and allocate the correct sizes for vectors
	size_t mid = inVec.size() / 2;
	vector<T> vec1;
	vec1.reserve(mid);
	vector<T> vec2;
	vec2.reserve(inVec.size() - mid);

	//copy the first half to vec1, second half to vec2
	auto iter = inVec.begin();
	for (; iter != inVec.begin() + mid; iter++)
	{
		vec1.push_back(*iter);
	}
	for (; iter != inVec.end(); iter++)
	{
		vec2.push_back(*iter);
	}

	//sort vec1, vec2
	vec1 = split_merge(vec1);
	vec2 = split_merge(vec2);

	//sort vec1 and vec2 into a new vector
	vector<T> retval;
	retval.reserve(vec1.size() + vec2.size());

	auto iter1 = vec1.end() - 1;
	auto iter2 = vec2.end() - 1;

	while (true)
	{
		if (vec1.empty())
		{
			for (size_t i = vec2.size() - 1; i > 0; i--)
			{
				retval.push_back(vec2[i]);
			}
			retval.push_back(vec2[0]);
			reverse(retval.begin(), retval.end());
			break;
		}
		if (vec2.empty())
		{
			for (size_t i = vec1.size() - 1; i > 0; i--)
			{
				retval.push_back(vec1[i]);
			}
			//the for loop will miss the first element
			retval.push_back(vec1[0]);
			reverse(retval.begin(), retval.end());
			break;
		}
		if (*iter1 > *iter2)
		{
			retval.push_back(*iter1);
			iter1 -= 1;
			vec1.pop_back();
		}
		else
		{
			retval.push_back(*iter2);
			iter2 -= 1;
			vec2.pop_back();
		}
	}

	//break
	return retval;
}

/**
 * 
 * */
template <typename T>
vector<T> my_quick_sort(const vector<T>& inVec)
{
	if (is_sorted(inVec.begin(), inVec.end()))
		return inVec;

	vector<T> retval = inVec;

	//start the pivot at the end of the list, and have a checker at the beginning
	auto pivot = retval.end() - 1;
	auto least = retval.begin();

	//do the quick sort recursively
	recursive_quick_sort(retval, least, pivot);

	return retval;
}

template <typename T>
void recursive_quick_sort(const vector<T> &inVec, typename vector<T>::iterator least, typename vector<T>::iterator pivot)
{
	//check to see if pivot and iter are the same, list is sorted
	if (least >= pivot)
	{
		return;
	}
	auto toBeSwapped = least;

	auto iter = least;
	while (iter != pivot)
	{
		if (*iter > *pivot)
		{
			toBeSwapped = iter;
			auto jter = pivot - 1;
			bool swapped = false;
			while (jter != iter)
			{
				if (*jter < *pivot)
				{
					swap(*jter, *iter);
					swapped = true;
					break;
				}
				jter--;
			}
			if (!swapped)
			{
				swap(*pivot, *toBeSwapped);
				recursive_quick_sort(inVec, least, toBeSwapped - 1);
				recursive_quick_sort(inVec, toBeSwapped + 1, pivot);
				return;
			}
		}
		iter++;
	}
	//swap(*pivot, *toBeSwapped);
	recursive_quick_sort(inVec, least, pivot - 1);
}

/**
 * Code for the partition and quickSort2 adapted from https://www.geeksforgeeks.org/quick-sort/
 * 
 * This function takes last element as pivot, places  
 * the pivot element at its correct position in sorted  
 * array, and places all smaller (smaller than pivot)  
 * to left of pivot and all greater elements to right  
 * of pivot 
 * */
template <typename T>
typename vector<T>::iterator partition(vector<T> &arr, typename vector<T>::iterator &low, typename vector<T>::iterator &high)
{
	T pivot = *high;							   // pivot
	typename vector<T>::iterator iter = (low - 1); // Index of smaller element

	for (typename vector<T>::iterator jter = low; jter != high; jter++)
	{
		// If current element is smaller than the pivot
		if (*jter < pivot)
		{
			iter++; // increment index of smaller element
			swap(*iter, *jter);
		}
	}
	iter++;
	swap(*iter, *high);
	return iter;
}

/** 
 * Code for the partition and quickSort2 adapted from https://www.geeksforgeeks.org/quick-sort/
 * 
 * The main function that implements QuickSort  
 * arr[] --> Array to be sorted
 * low --> Starting iterator
 * high --> Ending iterator 
 * */
template <typename T>
void quickSort2(vector<T> &arr, typename vector<T>::iterator low, typename vector<T>::iterator high)
{
	if (low < high)
	{
		// pi is partitioning index, arr[p] is now at right place
		typename vector<T>::iterator pi = partition(arr, low, high);

		// Separately sort elements before partition and after partition
		quickSort2(arr, low, pi - 1);
		quickSort2(arr, pi + 1, high);
	}
}

int main(int argc, char **argv)
{
	string filename;
	if (argc < 2)
	{
		filename = "/home/abiewer/Documents/current/algorithms/labs/l4/COMMUNISM.txt";
	}
	else
	{
		filename = argv[1];
	}

	vector<string> contents = put_file_contents_into_vector(filename);

	if (contents.size() == 0)
	{
		cout << "The file had nothing in it. " << endl;
		return 0;
	}

	int msStart, msEnd;

	cout << "The file being sorted is: " << filename << endl;

	//BUBBLE SORT
	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	vector<string> bubbleVec = my_bubble_sort(contents);
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	if (!is_sorted(bubbleVec.begin(), bubbleVec.end()))
		cout << "The bubble sort did not work" << endl;
	cout << "Computing the bubble sort 1 time took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	//MERGE SORT
	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	vector<string> mergeVec;
	for(int i = 0;i<10;i++){
		mergeVec = my_merge_sort(contents);
	}
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	if (!is_sorted(mergeVec.begin(), mergeVec.end()))
		cout << "The merge sort did not work" << endl;
	cout << "Computing the merge sort 10 times took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	//QUICK SORT
	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	vector<string> quickVec;
	for(int i = 0;i<10;i++){
		// quickVec = my_quick_sort(contents);
		quickVec = contents;
		quickSort2(quickVec, quickVec.begin(), quickVec.end()-1);
	}
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	if (!is_sorted(quickVec.begin(), quickVec.end()))
		cout << "The quick sort did not work" << endl;
	cout << "Computing the quick sort 10 times took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;

	//BUILT-IN SORTS
	msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	for(int i = 0;i<9;i++){
		vector<string> contents2 = contents;
		sort(contents2.begin(), contents2.end());
	}
	sort(contents.begin(), contents.end());
	msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	if (!is_sorted(contents.begin(), contents.end()))
		cout << "The built-in sort did not work" << endl;
	cout << "Computing the built-in sort 10 times took: " << ((double)(msEnd - msStart)) / 1000.0 << " seconds." << endl;


	vector<int> integerList {10, 5, 4, 7, 9, 2, 5, 8, 1};
	quickSort2(integerList, integerList.begin(), integerList.end()-1);
	if(!is_sorted(integerList.begin(), integerList.end()))
	{
		cout<<"The quick sort did not work"<<endl;
	}

	return 0;
}