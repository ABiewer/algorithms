	.file	"bracelet.cpp"
	.text
	.section	.text._ZnwmPv,"axG",@progbits,_ZnwmPv,comdat
	.weak	_ZnwmPv
	.type	_ZnwmPv, @function
_ZnwmPv:
.LFB38:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE38:
	.size	_ZnwmPv, .-_ZnwmPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.weak	_ZdlPvS_
	.type	_ZdlPvS_, @function
_ZdlPvS_:
.LFB40:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE40:
	.size	_ZdlPvS_, .-_ZdlPvS_
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
.LC0:
	.string	"stoi"
	.section	.text._ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi,"axG",@progbits,_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi,comdat
	.weak	_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi
	.type	_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi, @function
_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi:
.LFB881:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5c_strEv@PLT
	movq	%rax, %rsi
	movl	-20(%rbp), %edx
	movq	-16(%rbp), %rax
	movl	%edx, %r8d
	movq	%rax, %rcx
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	movq	strtol@GOTPCREL(%rip), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE881:
	.size	_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi, .-_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv,"axG",@progbits,_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv,comdat
	.align 2
	.weak	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	.type	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv, @function
_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv:
.LFB2489:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2489:
	.size	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv, .-_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	.section	.text._ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv,"axG",@progbits,_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv,comdat
	.align 2
	.weak	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	.type	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv, @function
_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv:
.LFB2491:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2491:
	.size	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv, .-_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	.section	.text._ZNSt16invalid_argumentC2ERKS_,"axG",@progbits,_ZNSt16invalid_argumentC5ERKS_,comdat
	.align 2
	.weak	_ZNSt16invalid_argumentC2ERKS_
	.type	_ZNSt16invalid_argumentC2ERKS_, @function
_ZNSt16invalid_argumentC2ERKS_:
.LFB2542:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt11logic_errorC2ERKS_@PLT
	leaq	16+_ZTVSt16invalid_argument(%rip), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2542:
	.size	_ZNSt16invalid_argumentC2ERKS_, .-_ZNSt16invalid_argumentC2ERKS_
	.weak	_ZNSt16invalid_argumentC1ERKS_
	.set	_ZNSt16invalid_argumentC1ERKS_,_ZNSt16invalid_argumentC2ERKS_
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED2Ev,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED2Ev
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED2Ev, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED2Ev:
.LFB2545:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2545:
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED2Ev, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED2Ev
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED1Ev
	.set	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED1Ev,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED2Ev
	.section	.text._ZNSt7__cxx114listIiSaIiEED2Ev,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEED5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEED2Ev
	.type	_ZNSt7__cxx114listIiSaIiEED2Ev, @function
_ZNSt7__cxx114listIiSaIiEED2Ev:
.LFB2548:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2548:
	.size	_ZNSt7__cxx114listIiSaIiEED2Ev, .-_ZNSt7__cxx114listIiSaIiEED2Ev
	.weak	_ZNSt7__cxx114listIiSaIiEED1Ev
	.set	_ZNSt7__cxx114listIiSaIiEED1Ev,_ZNSt7__cxx114listIiSaIiEED2Ev
	.section	.rodata
.LC1:
	.string	"Number of beads: "
.LC2:
	.string	"Number of colors: "
.LC3:
	.string	" ---- "
.LC4:
	.string	"The bracelets created:"
.LC5:
	.string	" bracelets were created."
	.align 8
.LC6:
	.string	" bracelets were duplicates, and "
.LC7:
	.string	" bracelets are unique."
.LC8:
	.string	"The program took "
.LC10:
	.string	" seconds to run."
.LC11:
	.string	"Invalid number of beads"
.LC12:
	.string	"Invalid number of colors"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2540:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2540
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cout(%rip), %rdi
.LEHB0:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
.LEHE0:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev@PLT
	leaq	-64(%rbp), %rax
	movq	%rax, %rsi
	leaq	_ZSt3cin(%rip), %rdi
.LEHB1:
	call	_ZSt7getlineIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE@PLT
.LEHE1:
	movl	$0, -200(%rbp)
	leaq	-64(%rbp), %rax
	movl	$10, %edx
	movl	$0, %esi
	movq	%rax, %rdi
.LEHB2:
	call	_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi
.LEHE2:
	movl	%eax, -200(%rbp)
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cout(%rip), %rdi
.LEHB3:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-64(%rbp), %rax
	movq	%rax, %rsi
	leaq	_ZSt3cin(%rip), %rdi
	call	_ZSt7getlineIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE@PLT
.LEHE3:
	movl	$0, -196(%rbp)
	leaq	-64(%rbp), %rax
	movl	$10, %edx
	movl	$0, %esi
	movq	%rax, %rdi
.LEHB4:
	call	_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi
.LEHE4:
	movl	%eax, -196(%rbp)
	call	_ZNSt6chrono3_V212system_clock3nowEv@PLT
	movq	%rax, -184(%rbp)
	leaq	-184(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	movq	%rax, -168(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, %rdi
.LEHB5:
	call	_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE
.LEHE5:
	movq	%rax, -176(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv
	movl	%eax, -192(%rbp)
	movl	$0, -220(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC1Ev
	movl	$0, -216(%rbp)
.L17:
	cvtsi2sd	-216(%rbp), %xmm2
	movsd	%xmm2, -232(%rbp)
	movl	-200(%rbp), %edx
	movl	-196(%rbp), %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	_ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_
	ucomisd	-232(%rbp), %xmm0
	seta	%al
	testb	%al, %al
	je	.L14
	movl	-216(%rbp), %eax
	movl	%eax, -212(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEEC1Ev
	addl	$1, -220(%rbp)
	movl	$0, -208(%rbp)
.L16:
	movl	-208(%rbp), %eax
	cmpl	-200(%rbp), %eax
	jge	.L15
	movl	-212(%rbp), %eax
	cltd
	idivl	-196(%rbp)
	movl	%edx, %eax
	movl	%eax, -168(%rbp)
	leaq	-168(%rbp), %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB6:
	call	_ZNSt7__cxx114listIiSaIiEE10push_frontEOi
	movl	-212(%rbp), %eax
	cltd
	idivl	-196(%rbp)
	movl	%eax, -212(%rbp)
	addl	$1, -208(%rbp)
	jmp	.L16
.L15:
	leaq	-96(%rbp), %rdx
	leaq	-160(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_
.LEHE6:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEED1Ev
	addl	$1, -216(%rbp)
	jmp	.L17
.L14:
	movl	-200(%rbp), %eax
	movl	%eax, %esi
	leaq	_ZSt4cout(%rip), %rdi
.LEHB7:
	call	_ZNSolsEi@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movl	-196(%rbp), %eax
	movl	%eax, %esi
	movq	%rdx, %rdi
	call	_ZNSolsEi@PLT
	movq	%rax, %rdx
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
	leaq	.LC4(%rip), %rsi
	leaq	_ZSt4cout(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
	movl	-220(%rbp), %eax
	movl	%eax, %esi
	leaq	_ZSt4cout(%rip), %rdi
	call	_ZNSolsEi@PLT
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
.LEHE7:
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv
	movq	%rax, -184(%rbp)
.L24:
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv
	movq	%rax, -168(%rbp)
	leaq	-168(%rbp), %rdx
	leaq	-184(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_
	testb	%al, %al
	je	.L18
	movq	-184(%rbp), %rax
	movq	%rax, -176(%rbp)
	leaq	-176(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi
.L23:
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv
	movq	%rax, -168(%rbp)
	leaq	-168(%rbp), %rdx
	leaq	-176(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_
	testb	%al, %al
	je	.L19
	leaq	-184(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv
	movq	%rax, %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB8:
	call	_ZNSt7__cxx114listIiSaIiEEC1ERKS2_
.LEHE8:
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv
	movq	%rax, %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB9:
	call	_ZNSt7__cxx114listIiSaIiEEC1ERKS2_
.LEHE9:
	movl	$0, -204(%rbp)
.L22:
	movl	-204(%rbp), %eax
	cmpl	-200(%rbp), %eax
	jge	.L20
	leaq	-96(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_
	testb	%al, %al
	je	.L21
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	leaq	-176(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi
	leaq	-168(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv
	movq	%rax, %rdx
	leaq	-160(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_
	nop
	jmp	.L20
.L21:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE4backEv
	movq	%rax, %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB10:
	call	_ZNSt7__cxx114listIiSaIiEE10push_frontERKi
.LEHE10:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE8pop_backEv
	addl	$1, -204(%rbp)
	jmp	.L22
.L20:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEED1Ev
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEED1Ev
	leaq	-176(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi
	jmp	.L23
.L19:
	leaq	-184(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi
	jmp	.L24
.L18:
	call	_ZNSt6chrono3_V212system_clock3nowEv@PLT
	movq	%rax, -184(%rbp)
	leaq	-184(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	movq	%rax, -176(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
.LEHB11:
	call	_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE
	movq	%rax, -168(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv
	movl	%eax, -188(%rbp)
	movl	-220(%rbp), %eax
	movslq	%eax, %rbx
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv
	subq	%rax, %rbx
	movq	%rbx, %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rdi
	call	_ZNSolsEm@PLT
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rbx
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZNSolsEm@PLT
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
	leaq	.LC8(%rip), %rsi
	leaq	_ZSt4cout(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movl	-188(%rbp), %eax
	subl	-192(%rbp), %eax
	cvtsi2sd	%eax, %xmm0
	movsd	.LC9(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movq	%rdx, %rdi
	call	_ZNSolsEd@PLT
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
.LEHE11:
	movl	$0, %ebx
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED1Ev
.L30:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev@PLT
	movl	%ebx, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L41
	jmp	.L52
.L43:
	movq	%rax, %rbx
	movq	%rdx, %rax
	cmpq	$1, %rax
	je	.L28
	jmp	.L29
.L28:
	movq	%rbx, %rax
	movq	%rax, %rdi
	call	__cxa_get_exception_ptr@PLT
	movq	%rax, %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16invalid_argumentC1ERKS_
	movq	%rbx, %rax
	movq	%rax, %rdi
	call	__cxa_begin_catch@PLT
	leaq	.LC11(%rip), %rsi
	leaq	_ZSt4cout(%rip), %rdi
.LEHB12:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
.LEHE12:
	movl	$1, %ebx
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt16invalid_argumentD1Ev@PLT
	call	__cxa_end_catch@PLT
	jmp	.L30
.L44:
	movq	%rax, %rbx
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt16invalid_argumentD1Ev@PLT
	call	__cxa_end_catch@PLT
	jmp	.L29
.L45:
	movq	%rax, %rbx
	movq	%rdx, %rax
	cmpq	$1, %rax
	je	.L34
	jmp	.L29
.L34:
	movq	%rbx, %rax
	movq	%rax, %rdi
	call	__cxa_get_exception_ptr@PLT
	movq	%rax, %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16invalid_argumentC1ERKS_
	movq	%rbx, %rax
	movq	%rax, %rdi
	call	__cxa_begin_catch@PLT
	leaq	.LC12(%rip), %rsi
	leaq	_ZSt4cout(%rip), %rdi
.LEHB13:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdx
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
.LEHE13:
	movl	$1, %ebx
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt16invalid_argumentD1Ev@PLT
	call	__cxa_end_catch@PLT
	jmp	.L30
.L46:
	movq	%rax, %rbx
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt16invalid_argumentD1Ev@PLT
	call	__cxa_end_catch@PLT
	jmp	.L29
.L47:
	movq	%rax, %rbx
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEED1Ev
	jmp	.L37
.L51:
	movq	%rax, %rbx
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEED1Ev
	jmp	.L39
.L50:
	movq	%rax, %rbx
.L39:
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEED1Ev
	movq	%rbx, %rax
	jmp	.L40
.L49:
.L40:
	movq	%rax, %rbx
	jmp	.L37
.L48:
	movq	%rax, %rbx
.L37:
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EED1Ev
	jmp	.L29
.L42:
	movq	%rax, %rbx
.L29:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB14:
	call	_Unwind_Resume@PLT
.LEHE14:
.L52:
	call	__stack_chk_fail@PLT
.L41:
	addq	$232, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2540:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
	.align 4
.LLSDA2540:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT2540-.LLSDATTD2540
.LLSDATTD2540:
	.byte	0x1
	.uleb128 .LLSDACSE2540-.LLSDACSB2540
.LLSDACSB2540:
	.uleb128 .LEHB0-.LFB2540
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB2540
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L42-.LFB2540
	.uleb128 0
	.uleb128 .LEHB2-.LFB2540
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L43-.LFB2540
	.uleb128 0x3
	.uleb128 .LEHB3-.LFB2540
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L42-.LFB2540
	.uleb128 0
	.uleb128 .LEHB4-.LFB2540
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L45-.LFB2540
	.uleb128 0x3
	.uleb128 .LEHB5-.LFB2540
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L42-.LFB2540
	.uleb128 0
	.uleb128 .LEHB6-.LFB2540
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L47-.LFB2540
	.uleb128 0
	.uleb128 .LEHB7-.LFB2540
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L48-.LFB2540
	.uleb128 0
	.uleb128 .LEHB8-.LFB2540
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L49-.LFB2540
	.uleb128 0
	.uleb128 .LEHB9-.LFB2540
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L50-.LFB2540
	.uleb128 0
	.uleb128 .LEHB10-.LFB2540
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L51-.LFB2540
	.uleb128 0
	.uleb128 .LEHB11-.LFB2540
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L48-.LFB2540
	.uleb128 0
	.uleb128 .LEHB12-.LFB2540
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L44-.LFB2540
	.uleb128 0
	.uleb128 .LEHB13-.LFB2540
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L46-.LFB2540
	.uleb128 0
	.uleb128 .LEHB14-.LFB2540
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
.LLSDACSE2540:
	.byte	0
	.byte	0
	.byte	0x1
	.byte	0x7d
	.align 4
	.long	DW.ref._ZTISt16invalid_argument-.
.LLSDATT2540:
	.text
	.size	main, .-main
	.section	.text._ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC2Ev,"axG",@progbits,_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC5Ev,comdat
	.align 2
	.weak	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC2Ev
	.type	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC2Ev, @function
_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC2Ev:
.LFB2553:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	__errno_location@PLT
	movl	(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, (%rax)
	call	__errno_location@PLT
	movl	$0, (%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2553:
	.size	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC2Ev, .-_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC2Ev
	.weak	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC1Ev
	.set	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC1Ev,_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC2Ev
	.section	.text._ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD2Ev,"axG",@progbits,_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD5Ev,comdat
	.align 2
	.weak	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD2Ev
	.type	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD2Ev, @function
_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD2Ev:
.LFB2556:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	__errno_location@PLT
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L56
	call	__errno_location@PLT
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, (%rdx)
.L56:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2556:
	.size	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD2Ev, .-_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD2Ev
	.weak	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD1Ev
	.set	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD1Ev,_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD2Ev
	.section	.text._ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE,"axG",@progbits,_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE,comdat
	.weak	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE
	.type	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE, @function
_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE:
.LFB2559:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	cmpq	$-2147483648, -8(%rbp)
	jl	.L58
	cmpq	$2147483647, -8(%rbp)
	jle	.L59
.L58:
	movl	$1, %eax
	jmp	.L60
.L59:
	movl	$0, %eax
.L60:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2559:
	.size	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE, .-_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE
	.section	.text._ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_,"axG",@progbits,_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_,comdat
	.weak	_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_
	.type	_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_, @function
_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_:
.LFB2551:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2551
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%r8d, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoC1Ev
	movl	-100(%rbp), %edx
	leaq	-40(%rbp), %rsi
	movq	-88(%rbp), %rcx
	movq	-72(%rbp), %rax
	movq	%rcx, %rdi
.LEHB15:
	call	*%rax
	movq	%rax, -32(%rbp)
	movq	-40(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L63
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt24__throw_invalid_argumentPKc@PLT
.L63:
	call	__errno_location@PLT
	movl	(%rax), %eax
	cmpl	$34, %eax
	je	.L64
	movq	-32(%rbp), %rax
	subq	$8, %rsp
	pushq	%rbx
	movq	%rax, %rdi
	call	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN10_Range_chk6_S_chkElSt17integral_constantIbLb1EE
	addq	$16, %rsp
	testb	%al, %al
	je	.L65
.L64:
	movl	$1, %eax
	jmp	.L66
.L65:
	movl	$0, %eax
.L66:
	testb	%al, %al
	je	.L67
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt20__throw_out_of_rangePKc@PLT
.LEHE15:
.L67:
	movq	-32(%rbp), %rax
	movl	%eax, -44(%rbp)
	cmpq	$0, -96(%rbp)
	je	.L68
	movq	-40(%rbp), %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	movq	%rax, %rdx
	movq	-96(%rbp), %rax
	movq	%rdx, (%rax)
.L68:
	movl	-44(%rbp), %ebx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD1Ev
	movl	%ebx, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L71
	jmp	.L73
.L72:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_EN11_Save_errnoD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB16:
	call	_Unwind_Resume@PLT
.LEHE16:
.L73:
	call	__stack_chk_fail@PLT
.L71:
	movq	-8(%rbp), %rbx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2551:
	.section	.gcc_except_table
.LLSDA2551:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2551-.LLSDACSB2551
.LLSDACSB2551:
	.uleb128 .LEHB15-.LFB2551
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L72-.LFB2551
	.uleb128 0
	.uleb128 .LEHB16-.LFB2551
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
.LLSDACSE2551:
	.section	.text._ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_,"axG",@progbits,_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_,comdat
	.size	_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_, .-_ZN9__gnu_cxx6__stoaIlicJiEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PmS9_
	.section	.text._ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC2IlvEERKT_,"axG",@progbits,_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC5IlvEERKT_,comdat
	.align 2
	.weak	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC2IlvEERKT_
	.type	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC2IlvEERKT_, @function
_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC2IlvEERKT_:
.LFB2797:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2797:
	.size	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC2IlvEERKT_, .-_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC2IlvEERKT_
	.weak	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC1IlvEERKT_
	.set	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC1IlvEERKT_,_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC2IlvEERKT_
	.section	.text._ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE,"axG",@progbits,_ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE,comdat
	.weak	_ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE
	.type	_ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE, @function
_ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE:
.LFB2795:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	movq	%rax, %rcx
	movabsq	$4835703278458516699, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	sarq	$18, %rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	movq	%rax, -24(%rbp)
	leaq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000EEEC1IlvEERKT_
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	je	.L77
	call	__stack_chk_fail@PLT
.L77:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2795:
	.size	_ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE, .-_ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE
	.section	.text._ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE,"axG",@progbits,_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE,comdat
	.weak	_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE
	.type	_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE, @function
_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE:
.LFB2794:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6chrono20__duration_cast_implINS_8durationIlSt5ratioILl1ELl1000EEEES2_ILl1ELl1000000EElLb1ELb0EE6__castIlS2_ILl1ELl1000000000EEEES4_RKNS1_IT_T0_EE
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2794:
	.size	_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE, .-_ZNSt6chrono13duration_castINS_8durationIlSt5ratioILl1ELl1000EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE
	.section	.text._ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv,"axG",@progbits,_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv,comdat
	.align 2
	.weak	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv
	.type	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv, @function
_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv:
.LFB2799:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv, .-_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000EEE5countEv
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC2Ev,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC2Ev
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC2Ev, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC2Ev:
.LFB2801:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2801:
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC2Ev, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC2Ev
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC1Ev
	.set	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC1Ev,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EEC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD2Ev
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD2Ev, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD2Ev:
.LFB2805:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2805:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD2Ev, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD2Ev
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD1Ev
	.set	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD1Ev,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD2Ev
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev:
.LFB2807:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implD1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2807:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED1Ev
	.set	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED1Ev,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EED2Ev
	.section	.text._ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_,"axG",@progbits,_ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_,comdat
	.weak	_ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_
	.type	_ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_, @function
_ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_:
.LFB2809:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cvtsi2sd	-8(%rbp), %xmm1
	cvtsi2sd	-4(%rbp), %xmm0
	call	pow@PLT
	movq	%xmm0, %rax
	movq	%rax, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2809:
	.size	_ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_, .-_ZSt3powIiiEN9__gnu_cxx11__promote_2IT_T0_NS0_9__promoteIS2_XsrSt12__is_integerIS2_E7__valueEE6__typeENS4_IS3_XsrS5_IS3_E7__valueEE6__typeEE6__typeES2_S3_
	.section	.text._ZNSt7__cxx114listIiSaIiEEC2Ev,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEEC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEEC2Ev
	.type	_ZNSt7__cxx114listIiSaIiEEC2Ev, @function
_ZNSt7__cxx114listIiSaIiEEC2Ev:
.LFB2811:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2811:
	.size	_ZNSt7__cxx114listIiSaIiEEC2Ev, .-_ZNSt7__cxx114listIiSaIiEEC2Ev
	.weak	_ZNSt7__cxx114listIiSaIiEEC1Ev
	.set	_ZNSt7__cxx114listIiSaIiEEC1Ev,_ZNSt7__cxx114listIiSaIiEEC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD2Ev
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD2Ev, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD2Ev:
.LFB2815:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeIiEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2815:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD2Ev, .-_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD2Ev
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD1Ev
	.set	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD1Ev,_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD2Ev
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEED2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEED5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev, @function
_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev:
.LFB2817:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implD1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2817:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev, .-_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEED1Ev
	.set	_ZNSt7__cxx1110_List_baseIiSaIiEED1Ev,_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev
	.section	.text._ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_,"axG",@progbits,_ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_,comdat
	.weak	_ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_
	.type	_ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_, @function
_ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_:
.LFB2820:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2820:
	.size	_ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_, .-_ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_
	.section	.text._ZNSt7__cxx114listIiSaIiEE10push_frontEOi,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE10push_frontEOi,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE10push_frontEOi
	.type	_ZNSt7__cxx114listIiSaIiEE10push_frontEOi, @function
_ZNSt7__cxx114listIiSaIiEE10push_frontEOi:
.LFB2819:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt4moveIRiEONSt16remove_referenceIT_E4typeEOS2_
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE5beginEv
	movq	%rax, %rcx
	movq	-24(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_
	nop
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2819:
	.size	_ZNSt7__cxx114listIiSaIiEE10push_frontEOi, .-_ZNSt7__cxx114listIiSaIiEE10push_frontEOi
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_:
.LFB2821:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv
	movq	%rax, %rcx
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2821:
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9push_backERKS2_
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv:
.LFB2822:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	(%rax), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC1EPNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L96
	call	__stack_chk_fail@PLT
.L96:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2822:
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv:
.LFB2823:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC1EPNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L99
	call	__stack_chk_fail@PLT
.L99:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2823:
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv
	.section	.text._ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_,"axG",@progbits,_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_,comdat
	.align 2
	.weak	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_
	.type	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_, @function
_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_:
.LFB2824:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2824:
	.size	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_, .-_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_
	.section	.text._ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi,"axG",@progbits,_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi
	.type	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi, @function
_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi:
.LFB2825:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L104
	call	__stack_chk_fail@PLT
.L104:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2825:
	.size	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi, .-_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEi
	.section	.text._ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv,"axG",@progbits,_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv,comdat
	.align 2
	.weak	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv
	.type	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv, @function
_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv:
.LFB2826:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2826:
	.size	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv, .-_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv
	.section	.text._ZNSt7__cxx114listIiSaIiEEC2ERKS2_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEEC5ERKS2_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEEC2ERKS2_
	.type	_ZNSt7__cxx114listIiSaIiEEC2ERKS2_, @function
_ZNSt7__cxx114listIiSaIiEEC2ERKS2_:
.LFB2828:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2828
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-40(%rbp), %rbx
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	movq	%rax, %rdx
	leaq	-25(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB17:
	call	_ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_
.LEHE17:
	leaq	-25(%rbp), %rax
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE
	leaq	-25(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeIiEED1Ev
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE3endEv
	movq	%rax, %rbx
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE5beginEv
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	subq	$8, %rsp
	pushq	%r12
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB18:
	.cfi_escape 0x2e,0x10
	call	_ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type
.LEHE18:
	addq	$16, %rsp
	nop
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L109
	jmp	.L111
.L110:
	movq	%rax, %rbx
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB19:
	call	_Unwind_Resume@PLT
.LEHE19:
.L111:
	call	__stack_chk_fail@PLT
.L109:
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2828:
	.section	.gcc_except_table
.LLSDA2828:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2828-.LLSDACSB2828
.LLSDACSB2828:
	.uleb128 .LEHB17-.LFB2828
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB18-.LFB2828
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L110-.LFB2828
	.uleb128 0
	.uleb128 .LEHB19-.LFB2828
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE2828:
	.section	.text._ZNSt7__cxx114listIiSaIiEEC2ERKS2_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEEC5ERKS2_,comdat
	.size	_ZNSt7__cxx114listIiSaIiEEC2ERKS2_, .-_ZNSt7__cxx114listIiSaIiEEC2ERKS2_
	.weak	_ZNSt7__cxx114listIiSaIiEEC1ERKS2_
	.set	_ZNSt7__cxx114listIiSaIiEEC1ERKS2_,_ZNSt7__cxx114listIiSaIiEEC2ERKS2_
	.section	.text._ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_,"axG",@progbits,_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_,comdat
	.weak	_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_
	.type	_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_, @function
_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_:
.LFB2830:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE4sizeEv
	movq	%rax, %rbx
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE4sizeEv
	cmpq	%rax, %rbx
	setne	%al
	testb	%al, %al
	je	.L113
	movl	$0, %eax
	jmp	.L121
.L113:
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE3endEv
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE3endEv
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE5beginEv
	movq	%rax, -40(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx114listIiSaIiEE5beginEv
	movq	%rax, -32(%rbp)
.L118:
	leaq	-56(%rbp), %rdx
	leaq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEneERKS0_
	testb	%al, %al
	je	.L115
	leaq	-48(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEneERKS0_
	testb	%al, %al
	je	.L115
	leaq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEdeEv
	movl	(%rax), %ebx
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEdeEv
	movl	(%rax), %eax
	cmpl	%eax, %ebx
	jne	.L115
	movl	$1, %eax
	jmp	.L116
.L115:
	movl	$0, %eax
.L116:
	testb	%al, %al
	je	.L117
	leaq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt20_List_const_iteratorIiEppEv
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt20_List_const_iteratorIiEppEv
	jmp	.L118
.L117:
	leaq	-56(%rbp), %rdx
	leaq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEeqERKS0_
	testb	%al, %al
	je	.L119
	leaq	-48(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEeqERKS0_
	testb	%al, %al
	je	.L119
	movl	$1, %eax
	jmp	.L120
.L119:
	movl	$0, %eax
.L120:
	nop
.L121:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L122
	call	__stack_chk_fail@PLT
.L122:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2830:
	.size	_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_, .-_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_
	.section	.text._ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi,"axG",@progbits,_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi
	.type	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi, @function
_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi:
.LFB2831:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L125
	call	__stack_chk_fail@PLT
.L125:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2831:
	.size	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi, .-_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEmmEi
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_:
.LFB2832:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE5beginEv
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE3endEv
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -40(%rbp)
.L131:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_
	testb	%al, %al
	je	.L127
	movq	-56(%rbp), %rax
	movq	%rax, -32(%rbp)
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv
	leaq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv
	movq	%rax, %rdx
	movq	-80(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZSteqIiSaIiEEbRKNSt7__cxx114listIT_T0_EES7_
	testb	%al, %al
	je	.L128
	leaq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEdeEv
	movq	%rax, %rdi
	call	_ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_
	movq	%rax, %rbx
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_
	cmpq	%rax, %rbx
	setne	%al
	testb	%al, %al
	je	.L129
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E
	jmp	.L128
.L129:
	movq	-56(%rbp), %rax
	movq	%rax, -40(%rbp)
.L128:
	movq	-32(%rbp), %rax
	movq	%rax, -56(%rbp)
	jmp	.L131
.L127:
	leaq	-48(%rbp), %rdx
	leaq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEneERKS4_
	testb	%al, %al
	je	.L134
	movq	-40(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E
.L134:
	nop
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L133
	call	__stack_chk_fail@PLT
.L133:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2832:
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE6removeERKS2_
	.section	.text._ZNSt7__cxx114listIiSaIiEE4backEv,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE4backEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE4backEv
	.type	_ZNSt7__cxx114listIiSaIiEE4backEv, @function
_ZNSt7__cxx114listIiSaIiEE4backEv:
.LFB2833:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE3endEv
	movq	%rax, -16(%rbp)
	leaq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorIiEmmEv
	leaq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorIiEdeEv
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L137
	call	__stack_chk_fail@PLT
.L137:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2833:
	.size	_ZNSt7__cxx114listIiSaIiEE4backEv, .-_ZNSt7__cxx114listIiSaIiEE4backEv
	.section	.text._ZNSt7__cxx114listIiSaIiEE10push_frontERKi,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE10push_frontERKi,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE10push_frontERKi
	.type	_ZNSt7__cxx114listIiSaIiEE10push_frontERKi, @function
_ZNSt7__cxx114listIiSaIiEE10push_frontERKi:
.LFB2834:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE5beginEv
	movq	%rax, %rcx
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2834:
	.size	_ZNSt7__cxx114listIiSaIiEE10push_frontERKi, .-_ZNSt7__cxx114listIiSaIiEE10push_frontERKi
	.section	.text._ZNSt7__cxx114listIiSaIiEE8pop_backEv,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE8pop_backEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE8pop_backEv
	.type	_ZNSt7__cxx114listIiSaIiEE8pop_backEv, @function
_ZNSt7__cxx114listIiSaIiEE8pop_backEv:
.LFB2835:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	8(%rax), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorIiEC1EPNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE
	nop
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L140
	call	__stack_chk_fail@PLT
.L140:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2835:
	.size	_ZNSt7__cxx114listIiSaIiEE8pop_backEv, .-_ZNSt7__cxx114listIiSaIiEE8pop_backEv
	.section	.text._ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv,"axG",@progbits,_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv,comdat
	.align 2
	.weak	_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv
	.type	_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv, @function
_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv:
.LFB2836:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2836
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2836:
	.section	.gcc_except_table
.LLSDA2836:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2836-.LLSDACSB2836
.LLSDACSB2836:
.LLSDACSE2836:
	.section	.text._ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv,"axG",@progbits,_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv,comdat
	.size	_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv, .-_ZNKSt7__cxx114listINS0_IiSaIiEEESaIS2_EE4sizeEv
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev:
.LFB2953:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC1Ev
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2953:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC1Ev
	.set	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC1Ev,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EEC2Ev
	.section	.text._ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev,"axG",@progbits,_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED5Ev,comdat
	.align 2
	.weak	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	.type	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev, @function
_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev:
.LFB2956:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2956:
	.size	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev, .-_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	.weak	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED1Ev
	.set	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED1Ev,_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv:
.LFB2958:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2958
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -24(%rbp)
.L147:
	movq	-40(%rbp), %rax
	cmpq	%rax, -24(%rbp)
	je	.L148
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv
	movq	%rax, -8(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_
	movq	-16(%rbp), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E
	jmp	.L147
.L148:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2958:
	.section	.gcc_except_table
.LLSDA2958:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2958-.LLSDACSB2958
.LLSDACSB2958:
.LLSDACSE2958:
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv,comdat
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE8_M_clearEv
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEEC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev, @function
_ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev:
.LFB2960:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC1Ev
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2960:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev, .-_ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEEC1Ev
	.set	_ZNSt7__cxx1110_List_baseIiSaIiEEC1Ev,_ZNSt7__cxx1110_List_baseIiSaIiEEC2Ev
	.section	.text._ZNSaISt10_List_nodeIiEED2Ev,"axG",@progbits,_ZNSaISt10_List_nodeIiEED5Ev,comdat
	.align 2
	.weak	_ZNSaISt10_List_nodeIiEED2Ev
	.type	_ZNSaISt10_List_nodeIiEED2Ev, @function
_ZNSaISt10_List_nodeIiEED2Ev:
.LFB2963:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2963:
	.size	_ZNSaISt10_List_nodeIiEED2Ev, .-_ZNSaISt10_List_nodeIiEED2Ev
	.weak	_ZNSaISt10_List_nodeIiEED1Ev
	.set	_ZNSaISt10_List_nodeIiEED1Ev,_ZNSaISt10_List_nodeIiEED2Ev
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv:
.LFB2965:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2965
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -24(%rbp)
.L153:
	movq	-40(%rbp), %rax
	cmpq	%rax, -24(%rbp)
	je	.L154
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeIiE9_M_valptrEv
	movq	%rax, -8(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_
	movq	-16(%rbp), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE
	jmp	.L153
.L154:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2965:
	.section	.gcc_except_table
.LLSDA2965:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2965-.LLSDACSB2965
.LLSDACSB2965:
.LLSDACSE2965:
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv,comdat
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv, .-_ZNSt7__cxx1110_List_baseIiSaIiEE8_M_clearEv
	.section	.text._ZNSt7__cxx114listIiSaIiEE5beginEv,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE5beginEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE5beginEv
	.type	_ZNSt7__cxx114listIiSaIiEE5beginEv, @function
_ZNSt7__cxx114listIiSaIiEE5beginEv:
.LFB2966:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	(%rax), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorIiEC1EPNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L157
	call	__stack_chk_fail@PLT
.L157:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2966:
	.size	_ZNSt7__cxx114listIiSaIiEE5beginEv, .-_ZNSt7__cxx114listIiSaIiEE5beginEv
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB2968:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2968:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_
	.type	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_, @function
_ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_:
.LFB2967:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	-24(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2967:
	.size	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_, .-_ZNSt7__cxx114listIiSaIiEE9_M_insertIJiEEEvSt14_List_iteratorIiEDpOT_
	.section	.text._ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE,"axG",@progbits,_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE,comdat
	.weak	_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE
	.type	_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE, @function
_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE:
.LFB2970:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2970:
	.size	_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE, .-_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_:
.LFB2969:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	-24(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2969:
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE9_M_insertIJRKS2_EEEvSt14_List_iteratorIS2_EDpOT_
	.section	.text._ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC2EPNSt8__detail15_List_node_baseE,"axG",@progbits,_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC5EPNSt8__detail15_List_node_baseE,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC2EPNSt8__detail15_List_node_baseE
	.type	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC2EPNSt8__detail15_List_node_baseE, @function
_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC2EPNSt8__detail15_List_node_baseE:
.LFB2972:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2972:
	.size	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC2EPNSt8__detail15_List_node_baseE, .-_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC2EPNSt8__detail15_List_node_baseE
	.weak	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC1EPNSt8__detail15_List_node_baseE
	.set	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC1EPNSt8__detail15_List_node_baseE,_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEC2EPNSt8__detail15_List_node_baseE
	.section	.text._ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv,"axG",@progbits,_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv,comdat
	.align 2
	.weak	_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv
	.type	_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv, @function
_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv:
.LFB2974:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2974:
	.size	_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv, .-_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv
	.section	.text._ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_,"axG",@progbits,_ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_,comdat
	.weak	_ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_
	.type	_ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_, @function
_ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_:
.LFB2975:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_
	movq	-24(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L169
	call	__stack_chk_fail@PLT
.L169:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2975:
	.size	_ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_, .-_ZN9__gnu_cxx14__alloc_traitsISaISt10_List_nodeIiEEE17_S_select_on_copyERKS3_
	.section	.text._ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv,"axG",@progbits,_ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv,comdat
	.align 2
	.weak	_ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	.type	_ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv, @function
_ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv:
.LFB2976:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2976:
	.size	_ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv, .-_ZNKSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEEC5ERKSaISt10_List_nodeIiEE,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE, @function
_ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE:
.LFB2978:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC1ERKSaISt10_List_nodeIiEE
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2978:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE, .-_ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEEC1ERKSaISt10_List_nodeIiEE
	.set	_ZNSt7__cxx1110_List_baseIiSaIiEEC1ERKSaISt10_List_nodeIiEE,_ZNSt7__cxx1110_List_baseIiSaIiEEC2ERKSaISt10_List_nodeIiEE
	.section	.text._ZNKSt7__cxx114listIiSaIiEE5beginEv,"axG",@progbits,_ZNKSt7__cxx114listIiSaIiEE5beginEv,comdat
	.align 2
	.weak	_ZNKSt7__cxx114listIiSaIiEE5beginEv
	.type	_ZNKSt7__cxx114listIiSaIiEE5beginEv, @function
_ZNKSt7__cxx114listIiSaIiEE5beginEv:
.LFB2980:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	(%rax), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt20_List_const_iteratorIiEC1EPKNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L175
	call	__stack_chk_fail@PLT
.L175:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2980:
	.size	_ZNKSt7__cxx114listIiSaIiEE5beginEv, .-_ZNKSt7__cxx114listIiSaIiEE5beginEv
	.section	.text._ZNKSt7__cxx114listIiSaIiEE3endEv,"axG",@progbits,_ZNKSt7__cxx114listIiSaIiEE3endEv,comdat
	.align 2
	.weak	_ZNKSt7__cxx114listIiSaIiEE3endEv
	.type	_ZNKSt7__cxx114listIiSaIiEE3endEv, @function
_ZNKSt7__cxx114listIiSaIiEE3endEv:
.LFB2981:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt20_List_const_iteratorIiEC1EPKNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L178
	call	__stack_chk_fail@PLT
.L178:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2981:
	.size	_ZNKSt7__cxx114listIiSaIiEE3endEv, .-_ZNKSt7__cxx114listIiSaIiEE3endEv
	.section	.text._ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type
	.type	_ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type, @function
_ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type:
.LFB2982:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
.L181:
	leaq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEneERKS0_
	testb	%al, %al
	je	.L182
	leaq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt20_List_const_iteratorIiEdeEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_
	leaq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt20_List_const_iteratorIiEppEv
	jmp	.L181
.L182:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2982:
	.size	_ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type, .-_ZNSt7__cxx114listIiSaIiEE22_M_initialize_dispatchISt20_List_const_iteratorIiEEEvT_S6_St12__false_type
	.section	.text._ZNKSt7__cxx114listIiSaIiEE4sizeEv,"axG",@progbits,_ZNKSt7__cxx114listIiSaIiEE4sizeEv,comdat
	.align 2
	.weak	_ZNKSt7__cxx114listIiSaIiEE4sizeEv
	.type	_ZNKSt7__cxx114listIiSaIiEE4sizeEv, @function
_ZNKSt7__cxx114listIiSaIiEE4sizeEv:
.LFB2983:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2983
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2983:
	.section	.gcc_except_table
.LLSDA2983:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2983-.LLSDACSB2983
.LLSDACSB2983:
.LLSDACSE2983:
	.section	.text._ZNKSt7__cxx114listIiSaIiEE4sizeEv,"axG",@progbits,_ZNKSt7__cxx114listIiSaIiEE4sizeEv,comdat
	.size	_ZNKSt7__cxx114listIiSaIiEE4sizeEv, .-_ZNKSt7__cxx114listIiSaIiEE4sizeEv
	.section	.text._ZNKSt20_List_const_iteratorIiEneERKS0_,"axG",@progbits,_ZNKSt20_List_const_iteratorIiEneERKS0_,comdat
	.align 2
	.weak	_ZNKSt20_List_const_iteratorIiEneERKS0_
	.type	_ZNKSt20_List_const_iteratorIiEneERKS0_, @function
_ZNKSt20_List_const_iteratorIiEneERKS0_:
.LFB2984:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2984:
	.size	_ZNKSt20_List_const_iteratorIiEneERKS0_, .-_ZNKSt20_List_const_iteratorIiEneERKS0_
	.section	.text._ZNKSt20_List_const_iteratorIiEdeEv,"axG",@progbits,_ZNKSt20_List_const_iteratorIiEdeEv,comdat
	.align 2
	.weak	_ZNKSt20_List_const_iteratorIiEdeEv
	.type	_ZNKSt20_List_const_iteratorIiEdeEv, @function
_ZNKSt20_List_const_iteratorIiEdeEv:
.LFB2985:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	_ZNKSt10_List_nodeIiE9_M_valptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2985:
	.size	_ZNKSt20_List_const_iteratorIiEdeEv, .-_ZNKSt20_List_const_iteratorIiEdeEv
	.section	.text._ZNSt20_List_const_iteratorIiEppEv,"axG",@progbits,_ZNSt20_List_const_iteratorIiEppEv,comdat
	.align 2
	.weak	_ZNSt20_List_const_iteratorIiEppEv
	.type	_ZNSt20_List_const_iteratorIiEppEv, @function
_ZNSt20_List_const_iteratorIiEppEv:
.LFB2986:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2986:
	.size	_ZNSt20_List_const_iteratorIiEppEv, .-_ZNSt20_List_const_iteratorIiEppEv
	.section	.text._ZNKSt20_List_const_iteratorIiEeqERKS0_,"axG",@progbits,_ZNKSt20_List_const_iteratorIiEeqERKS0_,comdat
	.align 2
	.weak	_ZNKSt20_List_const_iteratorIiEeqERKS0_
	.type	_ZNKSt20_List_const_iteratorIiEeqERKS0_, @function
_ZNKSt20_List_const_iteratorIiEeqERKS0_:
.LFB2987:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	sete	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2987:
	.size	_ZNKSt20_List_const_iteratorIiEeqERKS0_, .-_ZNKSt20_List_const_iteratorIiEeqERKS0_
	.section	.text._ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv,"axG",@progbits,_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv
	.type	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv, @function
_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv:
.LFB2988:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2988:
	.size	_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv, .-_ZNSt14_List_iteratorINSt7__cxx114listIiSaIiEEEEppEv
	.section	.text._ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_,"axG",@progbits,_ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_,comdat
	.weak	_ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_
	.type	_ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_, @function
_ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_:
.LFB2989:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2989:
	.size	_ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_, .-_ZSt11__addressofINSt7__cxx114listIiSaIiEEEEPT_RS4_
	.section	.text._ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_,"axG",@progbits,_ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_,comdat
	.weak	_ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_
	.type	_ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_, @function
_ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_:
.LFB2990:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2990:
	.size	_ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_, .-_ZSt11__addressofIKNSt7__cxx114listIiSaIiEEEEPT_RS5_
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E:
.LFB2991:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2991
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv
	movq	%rax, %rbx
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_
	movq	-40(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2991:
	.section	.gcc_except_table
.LLSDA2991:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2991-.LLSDACSB2991
.LLSDACSB2991:
.LLSDACSE2991:
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E,comdat
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE8_M_eraseESt14_List_iteratorIS2_E
	.section	.text._ZNSt7__cxx114listIiSaIiEE3endEv,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE3endEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE3endEv
	.type	_ZNSt7__cxx114listIiSaIiEE3endEv, @function
_ZNSt7__cxx114listIiSaIiEE3endEv:
.LFB2992:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorIiEC1EPNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L202
	call	__stack_chk_fail@PLT
.L202:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2992:
	.size	_ZNSt7__cxx114listIiSaIiEE3endEv, .-_ZNSt7__cxx114listIiSaIiEE3endEv
	.section	.text._ZNSt14_List_iteratorIiEmmEv,"axG",@progbits,_ZNSt14_List_iteratorIiEmmEv,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorIiEmmEv
	.type	_ZNSt14_List_iteratorIiEmmEv, @function
_ZNSt14_List_iteratorIiEmmEv:
.LFB2993:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2993:
	.size	_ZNSt14_List_iteratorIiEmmEv, .-_ZNSt14_List_iteratorIiEmmEv
	.section	.text._ZNKSt14_List_iteratorIiEdeEv,"axG",@progbits,_ZNKSt14_List_iteratorIiEdeEv,comdat
	.align 2
	.weak	_ZNKSt14_List_iteratorIiEdeEv
	.type	_ZNKSt14_List_iteratorIiEdeEv, @function
_ZNKSt14_List_iteratorIiEdeEv:
.LFB2994:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeIiE9_M_valptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2994:
	.size	_ZNKSt14_List_iteratorIiEdeEv, .-_ZNKSt14_List_iteratorIiEdeEv
	.section	.text._ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE,"axG",@progbits,_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE,comdat
	.weak	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE
	.type	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE, @function
_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE:
.LFB2996:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2996:
	.size	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE, .-_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE
	.section	.text._ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_
	.type	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_, @function
_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_:
.LFB2995:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	-24(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2995:
	.size	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_, .-_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_
	.section	.text._ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE
	.type	_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE, @function
_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE:
.LFB2997:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2997
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeIiE9_M_valptrEv
	movq	%rax, %rbx
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_
	movq	-40(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2997:
	.section	.gcc_except_table
.LLSDA2997:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2997-.LLSDACSB2997
.LLSDACSB2997:
.LLSDACSE2997:
	.section	.text._ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE,comdat
	.size	_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE, .-_ZNSt7__cxx114listIiSaIiEE8_M_eraseESt14_List_iteratorIiE
	.section	.text._ZNSt14_List_iteratorIiEC2EPNSt8__detail15_List_node_baseE,"axG",@progbits,_ZNSt14_List_iteratorIiEC5EPNSt8__detail15_List_node_baseE,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorIiEC2EPNSt8__detail15_List_node_baseE
	.type	_ZNSt14_List_iteratorIiEC2EPNSt8__detail15_List_node_baseE, @function
_ZNSt14_List_iteratorIiEC2EPNSt8__detail15_List_node_baseE:
.LFB2999:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2999:
	.size	_ZNSt14_List_iteratorIiEC2EPNSt8__detail15_List_node_baseE, .-_ZNSt14_List_iteratorIiEC2EPNSt8__detail15_List_node_baseE
	.weak	_ZNSt14_List_iteratorIiEC1EPNSt8__detail15_List_node_baseE
	.set	_ZNSt14_List_iteratorIiEC1EPNSt8__detail15_List_node_baseE,_ZNSt14_List_iteratorIiEC2EPNSt8__detail15_List_node_baseE
	.section	.text._ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv,"axG",@progbits,_ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv,comdat
	.align 2
	.weak	_ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv
	.type	_ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv, @function
_ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv:
.LFB3001:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt10_List_nodeImE9_M_valptrEv
	movq	(%rax), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3001:
	.size	_ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv, .-_ZNKSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE13_M_node_countEv
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC2Ev
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC2Ev, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC2Ev:
.LFB3039:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3039:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC2Ev, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC2Ev
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC1Ev
	.set	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC1Ev,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE10_List_implC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv:
.LFB3041:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3041
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	-8(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3041:
	.section	.gcc_except_table
.LLSDA3041:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3041-.LLSDACSB3041
.LLSDACSB3041:
.LLSDACSE3041:
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv,comdat
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE7_M_initEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev:
.LFB3043:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3043:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEED2Ev
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv:
.LFB3045:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3045:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_, @function
_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_:
.LFB3046:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3046:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_, .-_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE7destroyIS4_EEvRS6_PT_
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E:
.LFB3047:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3047
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movl	$1, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3047:
	.section	.gcc_except_table
.LLSDA3047:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3047-.LLSDACSB3047
.LLSDACSB3047:
.LLSDACSE3047:
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E,comdat
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_put_nodeEPSt10_List_nodeIS3_E
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2Ev
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2Ev, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2Ev:
.LFB3049:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeIiEEC2Ev
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3049:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2Ev, .-_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2Ev
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC1Ev
	.set	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC1Ev,_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv:
.LFB3051:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3051
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	-8(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3051:
	.section	.gcc_except_table
.LLSDA3051:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3051-.LLSDACSB3051
.LLSDACSB3051:
.LLSDACSE3051:
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv,comdat
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv, .-_ZNSt7__cxx1110_List_baseIiSaIiEE7_M_initEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev:
.LFB3053:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3053:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEED2Ev
	.section	.text._ZNSt10_List_nodeIiE9_M_valptrEv,"axG",@progbits,_ZNSt10_List_nodeIiE9_M_valptrEv,comdat
	.align 2
	.weak	_ZNSt10_List_nodeIiE9_M_valptrEv
	.type	_ZNSt10_List_nodeIiE9_M_valptrEv, @function
_ZNSt10_List_nodeIiE9_M_valptrEv:
.LFB3055:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3055:
	.size	_ZNSt10_List_nodeIiE9_M_valptrEv, .-_ZNSt10_List_nodeIiE9_M_valptrEv
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv:
.LFB3056:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3056:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv, .-_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_, @function
_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_:
.LFB3057:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3057:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_, .-_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE7destroyIiEEvRS2_PT_
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE:
.LFB3058:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3058
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movl	$1, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3058:
	.section	.gcc_except_table
.LLSDA3058:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3058-.LLSDACSB3058
.LLSDACSB3058:
.LLSDACSE3058:
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE,comdat
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE, .-_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_put_nodeEPSt10_List_nodeIiE
	.section	.text._ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_
	.type	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_, @function
_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_:
.LFB3059:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3059
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
.LEHB20:
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv
.LEHE20:
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	leaq	-48(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC1ERS2_PS1_
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	movq	%rax, %rbx
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeIiE9_M_valptrEv
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB21:
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_
.LEHE21:
	leaq	-48(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn
	movq	-64(%rbp), %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED1Ev
	movq	%rbx, %rax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L233
	jmp	.L235
.L234:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB22:
	call	_Unwind_Resume@PLT
.LEHE22:
.L235:
	call	__stack_chk_fail@PLT
.L233:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3059:
	.section	.gcc_except_table
.LLSDA3059:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3059-.LLSDACSB3059
.LLSDACSB3059:
	.uleb128 .LEHB20-.LFB3059
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB21-.LFB3059
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L234-.LFB3059
	.uleb128 0
	.uleb128 .LEHB22-.LFB3059
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE3059:
	.section	.text._ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_,comdat
	.size	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_, .-_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJiEEEPSt10_List_nodeIiEDpOT_
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm:
.LFB3060:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeImE9_M_valptrEv
	movq	(%rax), %rcx
	movq	-16(%rbp), %rdx
	addq	%rcx, %rdx
	movq	%rdx, (%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3060:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm, .-_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_inc_sizeEm
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_
	.type	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_, @function
_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_:
.LFB3061:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3061
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
.LEHB23:
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv
.LEHE23:
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE21_M_get_Node_allocatorEv
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	leaq	-48(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC1ERS6_PS5_
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE
	movq	%rax, %rbx
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeINSt7__cxx114listIiSaIiEEEE9_M_valptrEv
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB24:
	call	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_
.LEHE24:
	leaq	-48(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn
	movq	-64(%rbp), %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED1Ev
	movq	%rbx, %rax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L240
	jmp	.L242
.L241:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB25:
	call	_Unwind_Resume@PLT
.LEHE25:
.L242:
	call	__stack_chk_fail@PLT
.L240:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3061:
	.section	.gcc_except_table
.LLSDA3061:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3061-.LLSDACSB3061
.LLSDACSB3061:
	.uleb128 .LEHB23-.LFB3061
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB24-.LFB3061
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L241-.LFB3061
	.uleb128 0
	.uleb128 .LEHB25-.LFB3061
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSE3061:
	.section	.text._ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_,"axG",@progbits,_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_,comdat
	.size	_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_, .-_ZNSt7__cxx114listINS0_IiSaIiEEESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt10_List_nodeIS2_EDpOT_
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm:
.LFB3062:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeImE9_M_valptrEv
	movq	(%rax), %rcx
	movq	-16(%rbp), %rdx
	addq	%rcx, %rdx
	movq	%rdx, (%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3062:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_inc_sizeEm
	.section	.text._ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv:
.LFB3063:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3063:
	.size	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_, @function
_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_:
.LFB3064:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeIiEEC1ERKS1_
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3064:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_, .-_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE37select_on_container_copy_constructionERKS2_
	.section	.text._ZNSaISt10_List_nodeIiEEC2ERKS1_,"axG",@progbits,_ZNSaISt10_List_nodeIiEEC5ERKS1_,comdat
	.align 2
	.weak	_ZNSaISt10_List_nodeIiEEC2ERKS1_
	.type	_ZNSaISt10_List_nodeIiEEC2ERKS1_, @function
_ZNSaISt10_List_nodeIiEEC2ERKS1_:
.LFB3066:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3066:
	.size	_ZNSaISt10_List_nodeIiEEC2ERKS1_, .-_ZNSaISt10_List_nodeIiEEC2ERKS1_
	.weak	_ZNSaISt10_List_nodeIiEEC1ERKS1_
	.set	_ZNSaISt10_List_nodeIiEEC1ERKS1_,_ZNSaISt10_List_nodeIiEEC2ERKS1_
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2ERKSaISt10_List_nodeIiEE,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC5ERKSaISt10_List_nodeIiEE,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2ERKSaISt10_List_nodeIiEE
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2ERKSaISt10_List_nodeIiEE, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2ERKSaISt10_List_nodeIiEE:
.LFB3069:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeIiEEC2ERKS1_
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3069:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2ERKSaISt10_List_nodeIiEE, .-_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2ERKSaISt10_List_nodeIiEE
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC1ERKSaISt10_List_nodeIiEE
	.set	_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC1ERKSaISt10_List_nodeIiEE,_ZNSt7__cxx1110_List_baseIiSaIiEE10_List_implC2ERKSaISt10_List_nodeIiEE
	.section	.text._ZNSt20_List_const_iteratorIiEC2EPKNSt8__detail15_List_node_baseE,"axG",@progbits,_ZNSt20_List_const_iteratorIiEC5EPKNSt8__detail15_List_node_baseE,comdat
	.align 2
	.weak	_ZNSt20_List_const_iteratorIiEC2EPKNSt8__detail15_List_node_baseE
	.type	_ZNSt20_List_const_iteratorIiEC2EPKNSt8__detail15_List_node_baseE, @function
_ZNSt20_List_const_iteratorIiEC2EPKNSt8__detail15_List_node_baseE:
.LFB3072:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3072:
	.size	_ZNSt20_List_const_iteratorIiEC2EPKNSt8__detail15_List_node_baseE, .-_ZNSt20_List_const_iteratorIiEC2EPKNSt8__detail15_List_node_baseE
	.weak	_ZNSt20_List_const_iteratorIiEC1EPKNSt8__detail15_List_node_baseE
	.set	_ZNSt20_List_const_iteratorIiEC1EPKNSt8__detail15_List_node_baseE,_ZNSt20_List_const_iteratorIiEC2EPKNSt8__detail15_List_node_baseE
	.section	.text._ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_
	.type	_ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_, @function
_ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_:
.LFB3074:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE3endEv
	movq	%rax, %rcx
	movq	-24(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEE9_M_insertIJRKiEEEvSt14_List_iteratorIiEDpOT_
	nop
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3074:
	.size	_ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_, .-_ZNSt7__cxx114listIiSaIiEE12emplace_backIJRKiEEEvDpOT_
	.section	.text._ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv,"axG",@progbits,_ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv,comdat
	.align 2
	.weak	_ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv
	.type	_ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv, @function
_ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv:
.LFB3075:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt10_List_nodeImE9_M_valptrEv
	movq	(%rax), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3075:
	.size	_ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv, .-_ZNKSt7__cxx1110_List_baseIiSaIiEE13_M_node_countEv
	.section	.text._ZNKSt10_List_nodeIiE9_M_valptrEv,"axG",@progbits,_ZNKSt10_List_nodeIiE9_M_valptrEv,comdat
	.align 2
	.weak	_ZNKSt10_List_nodeIiE9_M_valptrEv
	.type	_ZNKSt10_List_nodeIiE9_M_valptrEv, @function
_ZNKSt10_List_nodeIiE9_M_valptrEv:
.LFB3076:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3076:
	.size	_ZNKSt10_List_nodeIiE9_M_valptrEv, .-_ZNKSt10_List_nodeIiE9_M_valptrEv
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm:
.LFB3077:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeImE9_M_valptrEv
	movq	(%rax), %rdx
	subq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3077:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_dec_sizeEm
	.section	.text._ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_
	.type	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_, @function
_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_:
.LFB3078:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3078
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
.LEHB26:
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv
.LEHE26:
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseIiSaIiEE21_M_get_Node_allocatorEv
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	leaq	-48(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC1ERS2_PS1_
	movq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rbx
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeIiE9_M_valptrEv
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB27:
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_
.LEHE27:
	leaq	-48(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn
	movq	-64(%rbp), %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED1Ev
	movq	%rbx, %rax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L260
	jmp	.L262
.L261:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB28:
	call	_Unwind_Resume@PLT
.LEHE28:
.L262:
	call	__stack_chk_fail@PLT
.L260:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3078:
	.section	.gcc_except_table
.LLSDA3078:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3078-.LLSDACSB3078
.LLSDACSB3078:
	.uleb128 .LEHB26-.LFB3078
	.uleb128 .LEHE26-.LEHB26
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB27-.LFB3078
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L261-.LFB3078
	.uleb128 0
	.uleb128 .LEHB28-.LFB3078
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
.LLSDACSE3078:
	.section	.text._ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_,"axG",@progbits,_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_,comdat
	.size	_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_, .-_ZNSt7__cxx114listIiSaIiEE14_M_create_nodeIJRKiEEEPSt10_List_nodeIiEDpOT_
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm:
.LFB3079:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeImE9_M_valptrEv
	movq	(%rax), %rdx
	subq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3079:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm, .-_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_dec_sizeEm
	.section	.text._ZNKSt10_List_nodeImE9_M_valptrEv,"axG",@progbits,_ZNKSt10_List_nodeImE9_M_valptrEv,comdat
	.align 2
	.weak	_ZNKSt10_List_nodeImE9_M_valptrEv
	.type	_ZNKSt10_List_nodeImE9_M_valptrEv, @function
_ZNKSt10_List_nodeImE9_M_valptrEv:
.LFB3080:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3080:
	.size	_ZNKSt10_List_nodeImE9_M_valptrEv, .-_ZNKSt10_List_nodeImE9_M_valptrEv
	.section	.text._ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev,"axG",@progbits,_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC5Ev,comdat
	.align 2
	.weak	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	.type	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev, @function
_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev:
.LFB3116:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3116:
	.size	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev, .-_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	.weak	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC1Ev
	.set	_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC1Ev,_ZNSaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm:
.LFB3118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeImE9_M_valptrEv
	movq	%rax, %rdx
	movq	-16(%rbp), %rax
	movq	%rax, (%rdx)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3118:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_set_sizeEm
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_:
.LFB3119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listIiSaIiEED1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3119:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE7destroyIS5_EEvPT_
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m, @function
_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m:
.LFB3120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3120:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m, .-_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m
	.section	.text._ZNSaISt10_List_nodeIiEEC2Ev,"axG",@progbits,_ZNSaISt10_List_nodeIiEEC5Ev,comdat
	.align 2
	.weak	_ZNSaISt10_List_nodeIiEEC2Ev
	.type	_ZNSaISt10_List_nodeIiEEC2Ev, @function
_ZNSaISt10_List_nodeIiEEC2Ev:
.LFB3122:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3122:
	.size	_ZNSaISt10_List_nodeIiEEC2Ev, .-_ZNSaISt10_List_nodeIiEEC2Ev
	.weak	_ZNSaISt10_List_nodeIiEEC1Ev
	.set	_ZNSaISt10_List_nodeIiEEC1Ev,_ZNSaISt10_List_nodeIiEEC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm:
.LFB3124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeImE9_M_valptrEv
	movq	%rax, %rdx
	movq	-16(%rbp), %rax
	movq	%rax, (%rdx)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3124:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm, .-_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_set_sizeEm
	.section	.text._ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv:
.LFB3125:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3125:
	.size	_ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_membufIiE6_M_ptrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_:
.LFB3126:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3126:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE7destroyIiEEvPT_
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m, @function
_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m:
.LFB3127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3127:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m, .-_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m
	.section	.text._ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv
	.type	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv, @function
_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv:
.LFB3128:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3128:
	.size	_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv, .-_ZNSt7__cxx1110_List_baseIiSaIiEE11_M_get_nodeEv
	.section	.text._ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC2ERS2_PS1_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC5ERS2_PS1_,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC2ERS2_PS1_
	.type	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC2ERS2_PS1_, @function
_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC2ERS2_PS1_:
.LFB3130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, 8(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3130:
	.size	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC2ERS2_PS1_, .-_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC2ERS2_PS1_
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC1ERS2_PS1_
	.set	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC1ERS2_PS1_,_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEC2ERS2_PS1_
	.section	.text._ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED5Ev,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED2Ev, @function
_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED2Ev:
.LFB3133:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L281
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movl	$1, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE10deallocateERS2_PS1_m
.L281:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3133:
	.size	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED2Ev, .-_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED1Ev
	.set	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED1Ev,_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_:
.LFB3135:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	movq	%rax, %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3135:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_, .-_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJiEEEvRS2_PT_DpOT0_
	.section	.text._ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn, @function
_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn:
.LFB3136:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3136:
	.size	_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt10_List_nodeIiEEEaSEDn
	.section	.text._ZNSt10_List_nodeImE9_M_valptrEv,"axG",@progbits,_ZNSt10_List_nodeImE9_M_valptrEv,comdat
	.align 2
	.weak	_ZNSt10_List_nodeImE9_M_valptrEv
	.type	_ZNSt10_List_nodeImE9_M_valptrEv, @function
_ZNSt10_List_nodeImE9_M_valptrEv:
.LFB3137:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3137:
	.size	_ZNSt10_List_nodeImE9_M_valptrEv, .-_ZNSt10_List_nodeImE9_M_valptrEv
	.section	.text._ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv
	.type	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv, @function
_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv:
.LFB3138:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3138:
	.size	_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv, .-_ZNSt7__cxx1110_List_baseINS_4listIiSaIiEEESaIS3_EE11_M_get_nodeEv
	.section	.text._ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC5ERS6_PS5_,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC2ERS6_PS5_
	.type	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC2ERS6_PS5_, @function
_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC2ERS6_PS5_:
.LFB3140:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, 8(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3140:
	.size	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC2ERS6_PS5_, .-_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC1ERS6_PS5_
	.set	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC1ERS6_PS5_,_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEC2ERS6_PS5_
	.section	.text._ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED5Ev,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED2Ev, @function
_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED2Ev:
.LFB3143:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L292
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movl	$1, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE10deallocateERS6_PS5_m
.L292:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED2Ev, .-_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED1Ev
	.set	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED1Ev,_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_:
.LFB3145:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE
	movq	%rax, %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3145:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_, .-_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE9constructIS4_JRKS4_EEEvRS6_PT_DpOT0_
	.section	.text._ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn, @function
_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn:
.LFB3146:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3146:
	.size	_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEaSEDn
	.section	.text._ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv:
.LFB3147:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3147:
	.size	_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_membufINSt7__cxx114listIiSaIiEEEE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC5ERKS3_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_:
.LFB3149:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3149:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC1ERKS3_
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC1ERKS3_,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2ERKS3_
	.section	.text._ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv,"axG",@progbits,_ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv
	.type	_ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv, @function
_ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv:
.LFB3151:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3151:
	.size	_ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv, .-_ZNK9__gnu_cxx16__aligned_membufIiE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_:
.LFB3152:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3152:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_, .-_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_
	.section	.text._ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv,"axG",@progbits,_ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv
	.type	_ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv, @function
_ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv:
.LFB3153:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3153:
	.size	_ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv, .-_ZNK9__gnu_cxx16__aligned_membufImE6_M_ptrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev:
.LFB3188:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3188:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m:
.LFB3190:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3190:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE10deallocateEPS6_m
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev:
.LFB3192:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3192:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEEC2Ev
	.section	.text._ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv:
.LFB3194:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3194:
	.size	_ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_membufIiE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m:
.LFB3195:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3195:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE10deallocateEPS2_m
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m, @function
_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m:
.LFB3196:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3196:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m, .-_ZNSt16allocator_traitsISaISt10_List_nodeIiEEE8allocateERS2_m
	.section	.text._ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_,comdat
	.weak	_ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_
	.type	_ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_, @function
_ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_:
.LFB3197:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3197:
	.size	_ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_, .-_ZSt11__addressofISaISt10_List_nodeIiEEEPT_RS3_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_:
.LFB3198:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	movl	(%rax), %ebx
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	movl	$4, %edi
	call	_ZnwmPv
	testq	%rax, %rax
	je	.L317
	movl	%ebx, (%rax)
.L317:
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3198:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJiEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv:
.LFB3199:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3199:
	.size	_ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_membufImE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m,comdat
	.weak	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m
	.type	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m, @function
_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m:
.LFB3200:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3200:
	.size	_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m, .-_ZNSt16allocator_traitsISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEE8allocateERS6_m
	.section	.text._ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_,comdat
	.weak	_ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_
	.type	_ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_, @function
_ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_:
.LFB3201:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3201:
	.size	_ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_, .-_ZSt11__addressofISaISt10_List_nodeINSt7__cxx114listIiSaIiEEEEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_:
.LFB3202:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3202
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKNSt7__cxx114listIiSaIiEEEEOT_RNSt16remove_referenceIS6_E4typeE
	movq	%rax, %r13
	movq	-48(%rbp), %r12
	movq	%r12, %rsi
	movl	$24, %edi
	call	_ZnwmPv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.L329
	movq	%r13, %rsi
	movq	%rbx, %rdi
.LEHB29:
	call	_ZNSt7__cxx114listIiSaIiEEC1ERKS2_
.LEHE29:
	jmp	.L329
.L328:
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZdlPvS_
	movq	%r13, %rax
	movq	%rax, %rdi
.LEHB30:
	call	_Unwind_Resume@PLT
.LEHE30:
.L329:
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3202:
	.section	.gcc_except_table
.LLSDA3202:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3202-.LLSDACSB3202
.LLSDACSB3202:
	.uleb128 .LEHB29-.LFB3202
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L328-.LFB3202
	.uleb128 0
	.uleb128 .LEHB30-.LFB3202
	.uleb128 .LEHE30-.LEHB30
	.uleb128 0
	.uleb128 0
.LLSDACSE3202:
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv,"axG",@progbits,_ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv
	.type	_ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv, @function
_ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv:
.LFB3203:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3203:
	.size	_ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv, .-_ZNK9__gnu_cxx16__aligned_membufIiE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_:
.LFB3204:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRKiEOT_RNSt16remove_referenceIS2_E4typeE
	movl	(%rax), %ebx
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	movl	$4, %edi
	call	_ZnwmPv
	testq	%rax, %rax
	je	.L335
	movl	%ebx, (%rax)
.L335:
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3204:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE9constructIiJRKiEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv,"axG",@progbits,_ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv
	.type	_ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv, @function
_ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv:
.LFB3205:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3205:
	.size	_ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv, .-_ZNK9__gnu_cxx16__aligned_membufImE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv:
.LFB3231:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv
	cmpq	%rax, -16(%rbp)
	seta	%al
	testb	%al, %al
	je	.L339
	call	_ZSt17__throw_bad_allocv@PLT
.L339:
	movq	-16(%rbp), %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, %rdi
	call	_Znwm@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3231:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8allocateEmPKv
	.section	.text._ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv:
.LFB3232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3232:
	.size	_ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_membufImE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv:
.LFB3233:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv
	cmpq	%rax, -16(%rbp)
	seta	%al
	testb	%al, %al
	je	.L344
	call	_ZSt17__throw_bad_allocv@PLT
.L344:
	movq	-16(%rbp), %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, %rdi
	call	_Znwm@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3233:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8allocateEmPKv
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv:
.LFB3242:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movabsq	$768614336404564650, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3242:
	.size	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeIiEE8max_sizeEv
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv:
.LFB3243:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movabsq	$461168601842738790, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3243:
	.size	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeINSt7__cxx114listIiSaIiEEEEE8max_sizeEv
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB3246:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L352
	cmpl	$65535, -8(%rbp)
	jne	.L352
	leaq	_ZStL8__ioinit(%rip), %rdi
	call	_ZNSt8ios_base4InitC1Ev@PLT
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rax
	movq	%rax, %rdi
	call	__cxa_atexit@PLT
.L352:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3246:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB3247:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3247:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.section	.rodata
	.align 8
.LC9:
	.long	0
	.long	1083129856
	.hidden	DW.ref._ZTISt16invalid_argument
	.weak	DW.ref._ZTISt16invalid_argument
	.section	.data.rel.local.DW.ref._ZTISt16invalid_argument,"awG",@progbits,DW.ref._ZTISt16invalid_argument,comdat
	.align 8
	.type	DW.ref._ZTISt16invalid_argument, @object
	.size	DW.ref._ZTISt16invalid_argument, 8
DW.ref._ZTISt16invalid_argument:
	.quad	_ZTISt16invalid_argument
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
