#include <iostream>
#include <vector>
#include <list>
#include <cmath>
#include <chrono>

using namespace std;

/*
Making bracelets from beads and colors

input:  n = number of beads
        k = number of colors

output: number of bracelets


*/

int main()
{
    //Get the number of beads
    cout << "Number of beads: ";
    string input;
    getline(cin, input);

    //try to make it a number
    int n = 0;
    try
    {
        n = stoi(input);
    }
    catch (invalid_argument)
    {
        cout << "Invalid number of beads" << endl;
        return 1;
    }
    //cout << endl;

    //Get the number of colors
    cout << "Number of colors: ";
    getline(cin, input);

    //Try to make it a number
    int k = 0;
    try
    {
        k = stoi(input);
    }
    catch (invalid_argument)
    {
        cout << "Invalid number of colors" << endl;
        return 1;
    }
    //cout << endl;

    int msStart = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

    int numBracelets = 0;

    //an array of each of the bracelets, the bracelets will be rearranged later, so they are lists
    list<list<int>> bracelets;

    //we are making every possible combination of beads (k^n)
    for (int i = 0; i < pow(k, n); i++)
    {
        //the count is the current position, so that it can be divided out to get the correct place value
        int count = i;
        //bracelets.push_back(list<int>());
        list<int> bracelet;
        numBracelets++;

        //for each bead position
        for (int j = 0; j < n; j++)
        {
            //use the current "binary" to get the next position
            bracelet.push_front(count % k);
            count = count / k;
        }

        bracelets.push_back(bracelet);
    }

    cout << n << " ---- " << k << endl;
    cout << "The bracelets created:" << endl;

    /*
    for (int i = 0; i < bracelet.size(); i++)
    {
        for (auto j = bracelet[i].begin(); j != bracelet[i].end(); j++)
        {
            cout << *j;
        }
        cout << endl;
    }
    */

    cout << numBracelets << " bracelets were created." << endl;

    //This is where the fun begins...
    //extern int parallelism_enabled;
    //#pragma omp parallel for
    for(auto iter = bracelets.begin(); iter!= bracelets.end(); iter++){
        auto jter = iter;
        jter++; //jter goes one after iter
        for(; jter!=bracelets.end();jter++)
        {
            list<int> b1 = *iter;
            list<int> b2 = *jter;
            for(int i = 0;i<n;i++)
            {
                if(b1==b2)
                {
                    auto toBeRemoved = jter;
                    jter--;
                    bracelets.remove(*toBeRemoved);
                    break;
                }
                b2.push_front(b2.back());
                b2.pop_back();
            }
        }
    }

    int msEnd = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();

    cout << numBracelets - bracelets.size() << " bracelets were duplicates, and "<< bracelets.size()<< " bracelets are unique."<<endl;

    cout<<"The program took " << ((double)(msEnd-msStart))/1000.0 << " seconds to run." <<endl;
    /*
    cout<<endl<<"Bracelets:"<<endl;
    for (auto i = bracelets.begin(); i!= bracelets.end(); i++)
    {
        for (auto j = (*i).begin(); j != (*i).end(); j++)
        {
            cout << *j;
        }
        cout << endl;
    }
    */

    return 0;
}