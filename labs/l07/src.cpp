/*
A debt-settling lab. 

The purpose of this lab is to take a list of debts from multiple people to others within the group and reorganize the debt so that there are the
fewest number of exchanges between people using a greedy algorithm. 



*/

#include "matrix.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <time.h>

using namespace std;

vector<int> read_row(string line)
{
	vector<int> retval;
	size_t pos;
	while(string::npos!=(pos = line.find(' ')))
	{
		retval.push_back(stoi(line));
		line = line.substr(pos+1);
	}
	retval.push_back(stoi(line));
	return retval;
}

vector<matrix> read_in_all_matricies(string filename)
{

	//the matrix file
	ifstream file(filename);

	//input/error checking
	if (!file)
	{
		throw "Could not open the file: " + filename;
	}

	vector<matrix> retval;
	matrix nextMat;

	//the current line of the file
	string line;
	//loop every line and get them into a vector of integers
	while (getline(file, line))
	{
		if(line == "" || line=="\r")
		{
			if(nextMat.empty())
				continue;
			//otherwise if the matrix is not empty, put it into the list of matricies and continue
			retval.push_back(nextMat);
			nextMat.clear();
			continue;
		}
		//take the current line in the file and convert it to a row in the matrix
		
		vector<int> row = read_row(line);
		//then store it
		nextMat.push_back(row);
	}
	//for when the file does not have an empty line at the end
	if(!nextMat.empty())
		retval.push_back(nextMat);

	file.close();

	return retval;
}

void generateRandomNumbers()
{
	srand(time(NULL));
	ofstream out("RandomGeneratedNumbers.txt");
	//10 matricies
	for(int mat = 0;mat<10000;mat++){
		int xmax, ymax;
		xmax = ymax = 5;
		matrix currentvals(xmax, vector<int>(ymax,0));

		//1-100 debts
		uint nmax = ((uint)rand())%(xmax*ymax)+1;
		for(uint n =0;n<nmax;n++)
		{
			uint x = ((uint)rand())%xmax;
			uint y = ((uint)rand())%ymax;
			if(x==y){
				n--;
				continue;
			}
			uint debt = ((uint)rand())%100;
			currentvals[x][y] = debt;
		}

		//copy matrix to file
		for(int row = 0;row<xmax;row++)
		{
			for(int col = 0;col<ymax-1;col++){
				out<<currentvals[row][col]<<" ";
			}
			out<<currentvals[row][ymax-1];
			out<<"\n";
		}
		out<<"\n";
	}

	out.close();
}

int main(int argc, char** argv)
{
	string filename;
	if(argc>1)
	{
		filename = argv[1];
	}
	else{
		filename = "RandomGeneratedNumbers.txt";
		generateRandomNumbers();
	}

	try{
		vector<matrix> matricies = read_in_all_matricies(filename);
		for(auto m : matricies){
			Matrix m1(m);
			if(!m1.isImproved())
			{
				cout<<"Original Debts: "<<endl;
				m1.printOriginalDebts();
				cout<<endl<<"Re-Balanced Debts:"<<endl;
				m1.printBalancedDebts();

				m1.printImprovement();
			}
		}
	}
	catch(invalid_argument){
		cout<<"There is an invalid integer in the matrix. "<<endl;
		return 1;
	}
	catch(out_of_range){
		cout<<"One of the integers in the matrix is too large to be processed. "<<endl;
		return 1;
	}
	catch(string err){
		cout<<err<<endl;
		return 1;
	}

	return 0;
}