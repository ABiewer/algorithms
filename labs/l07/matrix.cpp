#include "matrix.h"

using namespace std;

Matrix::Matrix(const string& filename){
	//the first thing to do is to read in the file and convert it to a matrix
	//the matrix file
	ifstream file(filename);

	//input/error checking
	if (!file)
	{
		cout << "Could not open the file: " << filename << endl;
		return;
	}

	//the current line of the file
	string line;
	//loop every line and get them into a vector of integers
	while (getline(file, line))
	{
		if(line == "")
			continue;
		//take the current line in the file and convert it to a row in the matrix
		
		vector<int> row = read_row(line);
		//then store it
		debtMatrix.push_back(row);
	}

	file.close();

	//this will take the matrix and calculate each person's debt to the group, and will also check to make sure the matrix 
	//is the correct dimensions
	calculate_debts();
	balanceDebts();
}

Matrix::Matrix(const matrix& inMatrix)
{
	//the matrix already exists, just need to convert it to the group debt
	debtMatrix = inMatrix;
	calculate_debts();
	balanceDebts();
}

void Matrix::calculate_debts()
{
	//first check to see if the data is the correct dimensions, and that each row has the correct number of elements
	size_t compVal = debtMatrix.size();
	for(size_t i = 0;i<debtMatrix.size();i++){
		if(debtMatrix[i].size()!=compVal){
			string err = "The row numbered: " + to_string(i) + " is not the same size as is should be.\nRow size: " + to_string(debtMatrix[i].size()) + ", column size: " + to_string(debtMatrix.size()) ;
			throw err;
		}
	}


	//to get the debts owed by each person, calculate the amount they owe across their row, and add the amount owed to them down their column
	for (size_t person = 0; person < debtMatrix.size(); person++)
	{
		size_t row, col;
		//the amount owed to the person - the amount they owe to others
		int amount = 0;

		//subtract the debts owed to others
		row = person;
		for (col = 0; col<debtMatrix[row].size();col++){
			amount -= debtMatrix[row][col];
		}

		//add the debts owed back
		col = person;
		for(row = 0;row<debtMatrix.size();row++){
			amount += debtMatrix[row][col];
		}

		//now the amount is the profit-debt, and we put it in the debts vector
		debts.push_back(amount);
	}
}

vector<int> Matrix::read_row(string line)
{
	vector<int> retval;
	size_t pos;
	while(string::npos!=(pos = line.find(' ')))
	{
		retval.push_back(stoi(line));
		line = line.substr(pos+1);
	}
	retval.push_back(stoi(line));
	return retval;
}

matrix Matrix::balanceDebts()
{
	//if we've already calculated the debts, we cannot change them, so just return the calculated values
	if(!balancedDebts.empty())
		return balancedDebts;

	//the total debt of all persons should total out to 0, so we just need to keep track of positive debt, once that reaches 0, we are done
	int totalDebt = 0;
	for(auto val : debts)
		if(val>0)
			totalDebt+=val;

	//make a new 2d matrix to hold the greedy algorithm's version of debt restructuring
	balancedDebts = matrix(debtMatrix.size(), vector<int>(debtMatrix[0].size(),0));

	while(totalDebt>0){
		//max debt will be the largest debt a person owes to the group
		int maxDebt = 0;
		//min debt will be the largest debt owed to a person in the group by all of the group
		int minDebt = 0;
		//and counters to track them
		size_t maxPerson = 0;
		size_t minPerson = 0;
		//find the largest/smallest debts
		for(size_t person = 0; person < debts.size(); person++){
			if(debts[person]>maxDebt)
			{
				maxDebt = debts[person];
				maxPerson = person;
			}
			else if(debts[person]<minDebt){
				minDebt = debts[person];
				minPerson = person;
			}
		}
		int debtVal;
		//determine whether to subtract the max debt from each person or to subtract the min debt, so that nobody swaps from negative to positive
		if(maxDebt>= -1*minDebt)
		{
			//the minimum debt is smaller in magnitude than the maximum debt, use it as the value for the debt exchange
			debtVal = -1 * minDebt;
		}
		else
		{
			//the maximum debt is smaller in magnitude than the minimum, use it as the exchange
			debtVal = maxDebt;
		}
		//acount for the debt
		balancedDebts[minPerson][maxPerson] = debtVal;
		//update the listed debts
		debts[maxPerson] -= debtVal;
		debts[minPerson] += debtVal;
		//change the counter
		totalDebt -= debtVal;
	}


	return balancedDebts;
}

void printMatrix(matrix printable){
	for(size_t i = 0;i<printable.size();i++){
		for(size_t j = 0;j<printable[i].size();j++){
			cout<<setw(4)<<printable[i][j]<<" ";
		}
		cout<<endl;
	}
}

void Matrix::printBalancedDebts(){
	printMatrix(balancedDebts);
}

void Matrix::printOriginalDebts(){
	printMatrix(debtMatrix);
}

vector<int> Matrix::getNumbers()
{
	int origNum = 0;
	int balancedNum = 0;

	for(size_t i = 0;i<balancedDebts.size();i++)
	{
		for(size_t j = 0;j<balancedDebts[i].size();j++)
		{
			if(balancedDebts[i][j]!=0)
				balancedNum++;
			if(debtMatrix[i][j]!=0)
				origNum++;
		}
	}
	vector<int> retval;
	retval.push_back(origNum);
	retval.push_back(balancedNum);
	return retval;
}

bool Matrix::isImproved()
{
	vector<int> nums = getNumbers();
	return(nums[0]>=nums[1]);
}

void Matrix::printImprovement(){
	vector<int> nums = getNumbers();
	int origNum = nums[0];
	int balancedNum = nums[1];

	cout<<"The original matrix had "<<origNum<<" exchanges, while the greedy algorithm has "<<balancedNum<<" exchanges. "<<endl;
	cout<<"This is a reduction of "<<100.0*(double)((double)(origNum-balancedNum)/(double)origNum)<<"%.\n";
}