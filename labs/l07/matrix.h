#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <climits>
#include <iomanip>

#define matrix vector<vector<int>>

using namespace std;

struct Matrix
{
private:
	matrix debtMatrix;
	matrix balancedDebts;
	vector<int> debts;

public:
	Matrix(const string& filename);
	Matrix(const matrix& inMatrix);
	matrix balanceDebts();

	void printBalancedDebts();
	void printOriginalDebts();
	void printImprovement();
	bool isImproved();

private:
	vector<int> read_row(string line);
	vector<int> getNumbers();
	void calculate_debts();
};