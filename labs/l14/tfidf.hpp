#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <math.h>

using namespace std;

class tfidf
{
public:

	tfidf(const string& filename);

private:
	//a list of all the unique words in the documents
	set<string> dictionary;
	//a container for the documents' contents
	vector<string> documents;

	//a map of the values for the idf of each word thru-out each document
	map<string, double> idf;
	//a map with the same indicies as 'documents' which gives the tf values for that document
	vector<map<string, double>> tf;

	//map of the number of documents which contain the word
	map<string, int> occurances;

	int numberOfDocuments;

	void readDictionary(ifstream& file);
	void findTFsForDocuments();
	void findIDFsForWords();
};