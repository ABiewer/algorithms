#include "tfidf.hpp"

tfidf::tfidf(const string &filename)
{
	ifstream file(filename);
	if (!file)
	{
		throw "Could not open the file: " + filename;
	}

	readDictionary(file);

	findTFsForDocuments();

	findIDFsForWords();

	//begin making the matrix
	cout << "done, dictionary size: " << dictionary.size() << " number of documents: " << numberOfDocuments << endl;
}

void tfidf::readDictionary(ifstream &file)
{
	
	string line;
	int counter = 0;

	while (getline(file, line))
	{
		if (line == "" || line == "\r")
			continue;
		counter++;

		if(line.back()=='\r')
			line.pop_back();

		documents.push_back(line);

		map<string, int> thisDocWords;

		//break the string apart on spaces, '\r'
		auto iter = find(line.begin(), line.end(), ' ');
		while (iter != line.end())
		{
			string word = line.substr(0, iter - line.begin());
			if (word == "" || word == "\r")
			{
				line = line.substr(iter - line.begin() + 1);
				iter = find(line.begin(), line.end(), ' ');
				continue;
			}

			dictionary.insert(word);
			if(find(thisDocWords.begin(), thisDocWords.end(), word)==thisDocWords.end())
			{
				occurances[word]++;
			}
			thisDocWords[word]++;

			line = line.substr(iter - line.begin() + 1);
			iter = find(line.begin(), line.end(), ' ');
		}

		string word = line.substr(0, iter - line.begin());
		if (word != "")
		{
			dictionary.insert(word);
			if(find(thisDocWords.begin(), thisDocWords.end(), word)==thisDocWords.end())
			{
				occurances[word]++;
			}
			thisDocWords[word]++;
		}
	}

	numberOfDocuments = counter;
	
}

void tfidf::findTFsForDocuments()
{
	for(auto docuiter = documents.begin(); docuiter!=documents.end(); docuiter++)
	{
		
		map<string, int> tfInts;
		map<string,double> tfList;

		//for every word in the document, count up how many times it appears, divide by the number of words in the document. 
		//TODO working on this... the idf's are finished, just need to create a vector containing the tf's for each word in each document
		//then multiply them together and dot product for the specified document against each other document
		//if the amount of memory ends up being a problem, just don't finish this

		string document = *docuiter;
		auto iter = find(document.begin(), document.end(), ' ');
		int wordCount = 0;
		while(iter!=document.end())
		{
			string word = string(document.begin(), iter);
			tfInts[word]++;
			wordCount++;
			document = string(iter+1, document.end());
			iter = find(document.begin(), document.end(), ' ');
		}
		tfInts[document]++;
		wordCount++;

		for(auto iter = tfInts.begin(); iter!= tfInts.end(); iter++)
		{
			tfList[iter->first] = (double)(iter->second)/(double)(wordCount);
		}

		tf.push_back(tfList);
		
	}
}

void tfidf::findIDFsForWords()
{
	for(auto iter = occurances.begin();iter!=occurances.end();iter++){
		idf[iter->first] = log((double)(1+numberOfDocuments)/(double)(1+iter->second));
	}
}