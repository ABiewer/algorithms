/*
The purpose of this lab is to create a matrix class that we can use to
perform matrix operations for later in the course.
Your program should be able to add, subtract, multiply, and compute powers of a
matrix (a matrix multiplied by itself k times) when it is admissible to do so.

Input Files: The input file for this lab will contain 7 matrices. The entries in the
row of a matrix will be separated by spaces and the rows will be separated by newlines.
Breaks between matrices will be indicated by the blank lines, as shown in the following
example.
7 3 2 4
-3 5 0 -1

1 -2
3 4

8 -2 -3 5
12 -4 -4 6

Here are the tasks to be computed by your program, if possible.
1. Add the 1st and 2nd matrices.
2. Add the 1st and 3rd matrices.
3. Subtract the 6th and 7th matrices.
4. Multiply the 1st and 3rd matrices.
5. Multiply the 2nd and 3rd matrices.
6. Multiply the 2nd and 4th matrices.
7. Compute the 29th power of the 5th matrix.
8. Multiply the 6th and 7th matrices.

Output: Your output should be the result of performing the computations above,
if possible. If a task is not possible, your program should indicate the error. Due
to the size of the matrices in the file, please output your results to a file, in order
to not loose information on the screen. Using the examples above, what is printed
to the file for parts 1 and 2 would be:
Error: The first two matrices cannot be added
(they do not have the same dimensions).
The sum of the first and third matrices is
15 1 -1 9
9 1 -4 5
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

vector<int> convert_line(const string &line);
vector<int> convert_line(const string &line);

template <typename T>
struct MyMatrix
{
public:
    static int matrixNumber;
    vector<vector<T>> matrix;
    int rows, cols;

    MyMatrix(const vector<vector<T>> &input)
    {
        matrix = input;
        //matricies should be uniform in size in each dimension, but we only need to check the length of the rows

        for (auto iter = matrix.begin() + 1; iter != matrix.end(); iter++)
            if ((*iter).size() != matrix[0].size())
                throw "The dimensions of the matrix are not consistent\n";
        rows = (int)matrix.size();
        cols = (int)matrix[0].size();
    }

    MyMatrix(const MyMatrix<T> &old)
    {
        matrix = old.matrix; //vector copy constructor ftw.
        cols = old.cols;
        rows = old.rows;
    }

    MyMatrix()
    {
        rows = cols = 0;
    }

    void print_matrix()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                cout << matrix[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }

    void print_matrix(ofstream &file)
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                file << matrix[i][j] << " ";
            }
            file << endl;
        }
        file << endl;
    }

    MyMatrix<T> power(int exponent) //technically a O(n^4) algorithm
    {
        if (rows != cols)
        {
            throw "Could not compute the power of the matrix. The sides are not the same dimension.\n";
        }
        MyMatrix<T> temp = *this;
        for (int i = 1; i < exponent; i++)
        {
            temp *= temp;
        }
        return temp;
    }
};

template <typename T>
MyMatrix<T> operator+(const MyMatrix<T> &lhs, const MyMatrix<T> &rhs)
{
    if (lhs.rows != rhs.rows || lhs.cols != rhs.cols)
    {
        throw "The rows and columns of the two matricies do not match.\n";
        return MyMatrix<T>();
    }
    vector<vector<T>> newMatrix;
    for (int i = 0; i < lhs.rows; i++)
    {
        newMatrix.push_back(vector<T>());
        for (int j = 0; j < lhs.cols; j++)
        {
            newMatrix[i].push_back(lhs.matrix[i][j] + rhs.matrix[i][j]);
        }
    }
    return MyMatrix<T>(newMatrix);
}

template <typename T>
MyMatrix<T> &operator+=(MyMatrix<T> &lhs, const MyMatrix<T> &rhs)
{
    lhs = lhs + rhs;
    return lhs;
}

template <typename T>
MyMatrix<T> operator-(const MyMatrix<T> &lhs, const MyMatrix<T> &rhs)
{
    if (lhs.rows != rhs.rows || lhs.cols != rhs.cols)
    {
        throw "The rows and columns of the two matricies do not match.\n";
        return MyMatrix<T>();
    }
    vector<vector<T>> newMatrix;
    for (int i = 0; i < lhs.rows; i++)
    {
        newMatrix.push_back(vector<T>());
        for (int j = 0; j < lhs.cols; j++)
        {
            newMatrix[i].push_back(lhs.matrix[i][j] - rhs.matrix[i][j]);
        }
    }
    return MyMatrix<T>(newMatrix);
}

template <typename T>
MyMatrix<T> &operator-=(MyMatrix<T> &lhs, const MyMatrix<T> &rhs)
{
    lhs = lhs - rhs;
    return lhs;
}

template <typename T>
MyMatrix<T> operator*(const MyMatrix<T> &lhs, const MyMatrix<T> &rhs)
{
    if (lhs.cols != rhs.rows)
    {
        throw "The matricies cannot be multiplied together.\n";
        return MyMatrix<T>();
    }
    vector<vector<T>> newMatrix;
    for (int i = 0; i < lhs.rows; i++)
    {
        newMatrix.push_back(vector<T>());
        for (int j = 0; j < rhs.cols; j++) //very important rhs.cols, not lhs.cols
        {
            T sum = T();
            for (int k = 0; k < lhs.cols; k++)
            {
                sum += lhs.matrix[i][k] * rhs.matrix[k][j];
            }
            newMatrix[i].push_back(sum);
        }
    }
    return MyMatrix<T>(newMatrix);
}

template <typename T>
MyMatrix<T> &operator*=(MyMatrix<T> &lhs, const MyMatrix<T> &rhs)
{
    lhs = lhs * rhs;
    return lhs;
}

vector<MyMatrix<int>> read_in_matricies(string filename)
{
    ifstream file(filename);
    if (!file)
    {
        throw "Could not open the file: " + filename + "\n";
    }
    string line;
    vector<MyMatrix<int>> retvals;
    vector<vector<int>> matrix;
    while (getline(file, line))
    {
        if (line == "" || line == "\r")
        {
            retvals.push_back(MyMatrix<int>(matrix));
            matrix.clear();
            continue;
        }
        vector<int> row = convert_line(line);
        matrix.push_back(row);
    }
    if (!matrix.empty())
    {
        retvals.push_back(matrix);
    }

    file.close();
    return retvals;
}

vector<int> convert_line(const string &line)
{
    string tempstr = line;
    vector<int> retval;
    while (tempstr != "")
    {
        try
        {
            retval.push_back(stoi(tempstr));
            auto iter = find(tempstr.begin(), tempstr.end(), ' ');
            if (iter != tempstr.end())
            {
                tempstr.erase(tempstr.begin(), find(tempstr.begin(), tempstr.end(), ' ') + 1);
            }
            else
            {
                break;
            }
        }
        catch (invalid_argument)
        {
            cout << "Could not convert: " << line << " to integers. " << endl;
        }
    }
    return retval;
}

int main(int argc, char **argv)
{
    string filename = "/home/abiewer/Downloads/MATRIXLIST.txt";
    vector<MyMatrix<int>> matricies;
    try
    {
        matricies = read_in_matricies(filename);
    }
    catch (const char *err)
    {
        cout << err << endl;
        return 1;
    }
    /*
    for(MyMatrix<int> m : matricies){
        m.print_matrix();
    }*/

    /*
    Here are the tasks to be computed by your program, if possible.
    1. Add the 1st and 2nd matrices.
    2. Add the 1st and 3rd matrices.
    3. Subtract the 6th and 7th matrices.
    4. Multiply the 1st and 3rd matrices.
    5. Multiply the 2nd and 3rd matrices.
    6. Multiply the 2nd and 4th matrices.
    7. Compute the 29th power of the 5th matrix.
    8. Multiply the 6th and 7th matrices.
    */

    ofstream file("output.txt");

    try
    {
        //1. Add the 1st and 2nd matrices.
        MyMatrix<int> answer0 = matricies[0] + matricies[1];
        answer0.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    try
    {
        //2. Add the 1st and 3rd matrices.
        MyMatrix<int> answer1 = matricies[0] + matricies[2];
        answer1.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    try
    {
        //3. Subtract the 6th and 7th matrices.
        MyMatrix<int> answer2 = matricies[6] - matricies[5];
        answer2.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    try
    {
        //4. Multiply the 1st and 3rd matrices.
        MyMatrix<int> answer2 = matricies[0] * matricies[2];
        answer2.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    try
    {
        //5. Multiply the 2nd and 3rd matrices.
        MyMatrix<int> answer2 = matricies[1] * matricies[2];
        answer2.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    try
    {
        //6. Multiply the 2nd and 4th matrices.
        MyMatrix<int> answer2 = matricies[1] * matricies[3];
        answer2.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    try
    {
        //7. Compute the 29th power of the 5th matrix.
        MyMatrix<int> answer2 = matricies[4].power(29);
        answer2.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    try
    {
        //8. Multiply the 6th and 7th matrices.
        MyMatrix<int> answer2 = matricies[5] * matricies[6];
        answer2.print_matrix(file);
    }
    catch (const char *err)
    {
        file << err << endl;
    }

    file.close();

    return 0;
}