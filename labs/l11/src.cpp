/*
Objective: In this lab, you will write code that can both encode and decode messages
via a Caesar cipher. In the case of encoding the cipher, you should ask the user via a
console window for the multiplicative term and the additive term of the cipher. In the
case of decoding a cipher, your program should crack the code as discussed in the video
(attempting all possible decodes and checking for a certain percentage of words to be in
the dictionary).

Input File: For consistency, the input file will be a keyword (either ENCODE or
DECODE) followed by a string message. For the purposes of this lab, I will NOT attempt
to throw you off with a first word that is anything other than ENCODE or DECODE,
unless it is part of a message. An example of an input text is given below.

ENCODE: Why do melons have to have formal marriages? Because they can’t elope!
DECODE: xibu’t b npvtf’t gbwpsjuf hbnf? ijef boe trvffl!

You should be careful, however, of capital letters and other characters within the
input text (we want to preserve spaces and formatting).

Output: If encoding the top message with a multiplicative term of 3 and additive
term of 1, the encoded message would be:
pwv kr lnirod wbmn gr wbmn qralbi lbaazbtnd? enhbjdn gwnv hbo’g nirun!

While the decoded message would be:
what’s a mouse’s favorite game? hide and squeek!

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

using namespace std;

vector<string> readInDictionary(const string& filename)
{
	vector<string> retval;

	ifstream file(filename);
	if(!file){
		throw "Could not open the file: " + filename;
	}

	string line;
	while(getline(file, line))
	{
		if(line.back() == '\r')
			line.pop_back();
		if(line=="")
			continue;
		retval.push_back(line);
	}

	return retval;
}

//reads in the file, looking for any lines that start with encode or decode and returns them.
vector<string> readInFile(const string &filename)
{
	vector<string> retval;

	ifstream file(filename);
	if (!file)
	{
		throw "The file: " + filename + " could not be opened. ";
	}

	string line;
	while (getline(file, line))
	{
		string front = line.substr(0, 6);
		string encode = "encode";
		string decode = "decode";
		std::transform(front.begin(), front.end(), front.begin(), [](unsigned char c) { return tolower(c); });

		if (front == encode || front == decode)
		{
			retval.push_back(line);
		}
	}

	file.close();

	return retval;
}

vector<int> readInFileCoefficients(string filename)
{
	vector<int> retval;

	ifstream file(filename);
	if (!file)
	{
		throw "The file: " + filename + "could not be opened. ";
	}

	string line;
	while(getline(file, line)){
		try{
			retval.push_back(stoi(line));
		}
		catch(...)
		{
		}
	}

	return retval;
}

void encodeLine(string line, int m, int b);

void encodeLine(string line)
{
	int m, b;
	cout << "How would you like to encode the line: " + line + "\nPlease enter a value for m: ";
	string mstring;
	getline(cin, mstring);

	try
	{
		m = stoi(mstring);
	}
	catch (...)
	{
		cout << "Invalid response. " << endl;
		return;
	}

	cout << "Please enter a value for b: ";
	string bstring;
	getline(cin, bstring);

	try
	{
		b = stoi(bstring);
	}
	catch (...)
	{
		cout << "Invalid response. " << endl;
		return;
	}

	encodeLine(line, m, b);
}

void encodeLine(string line, int m, int b)
{
	for (size_t i = 0; i < line.size(); i++)
	{
		int curr = line[i] - 'a';
		if (curr < 0 || curr > 25)
			continue;
		curr = (m * curr + b) % 26;
		line[i] = curr + 'a';
	}

	cout << "Encoded message: " << endl;
	cout << line << endl
		 << endl;
	return;
}

bool goodDecryption(string& line, const vector<string>& dictionary, int m, int b)
{
	//find m's inverse mod 26
	for(int i = 1;i<26;i++){
		if((m*i)%26 == 1)
		{
			m = i;
			break;
		}
	}

	for (size_t i = 0; i < line.size(); i++)
	{
		int curr = line[i] - 'a';
		if (curr < 0 || curr > 25)
			continue;
		//subtract b, if the number is a multiple of m, divide, otherwise add 26 then divide
		curr = (curr - b+26)%26;
		curr = (curr*m)%26;

		line[i] = curr + 'a';
	}

	//split the line on punctuation
	vector<string> words;

	boost::algorithm::split(words, line, boost::algorithm::is_any_of(" '.?,/\\\r"));

	int goodWords = 0;
	int badWords = 0;
	for(auto i : words)
	{
		if(i=="")
			continue;
		if(binary_search(dictionary.begin(), dictionary.end(), i))
			goodWords++;
		else
			badWords++;
	}
	if((double)goodWords/(double)(goodWords+badWords) > .5)
	{
		return true;
	}
	return false;
	
}

void decodeLine(string& line, const vector<string>& dictionary){

	for(int m = 1;m<26;m++)
	{
		if(m==13 || m==2)
			continue;
		for(int b = 0;b<26;b++){
			string decryption = line;
			if(goodDecryption(decryption, dictionary, m, b))
			{
				cout<<"With m = "<<m<<" and b = "<<b<<", the message decrypts to: "<<endl;
				cout<<decryption<<endl;
			}
		}
	}
}

int main(int argc, char **argv)
{
	vector<string> dictionary;

	try{
		dictionary = readInDictionary("Dictionary.txt");
	}
	catch(string err)
	{
		cout<<err<<endl;
		return 1;
	}	

	vector<string> inputs;
	vector<bool> encode;

	string filename = "input.txt";
	if (argc > 1)
		filename = argv[1];

	try
	{
		inputs = readInFile(filename);
	}
	catch (string err)
	{
		cout << err << endl;
		return 1;
	}

	for (auto i = inputs.begin(); i != inputs.end(); i++)
	{
		string &line = *i;

		string front = line.substr(0, 6);
		std::transform(front.begin(), front.end(), front.begin(), [](unsigned char c) { return tolower(c); });

		if (front == "encode")
			encode.push_back(true);
		else
			encode.push_back(false);

		//remove the encode or decode
		line = line.substr(6);
		//remove any extra spacing or formating from the beginning
		while (line[0] == ' ' || line[0] == ':')
			line = line.substr(1);
	}

	/*
	for (auto i : inputs)
		cout << i << endl;
	*/

	if (argc > 2)
	{
		filename = argv[2];

		vector<int> coefficients;
		try
		{
			coefficients = readInFileCoefficients(filename);
		}
		catch (string err)
		{
			cout << err << endl;
			return 1;
		}

		for (size_t i = 0; i < encode.size(); i++)
		{
			if (encode[i])
			{
				encodeLine(inputs[i], coefficients[2*i], coefficients[2*i+1]);
			}
			else{
				decodeLine(inputs[i], dictionary);
			}
		}
	}
	else
	{
		for (size_t i = 0; i < encode.size(); i++)
		{
			if (encode[i])
			{
				encodeLine(inputs[i]);
			}
			else{
				decodeLine(inputs[i], dictionary);
			}
		}
	}

	return 0;
}